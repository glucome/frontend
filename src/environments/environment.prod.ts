export const environment = {
  production: true,
  API_URL: "/api/v1",
  BACKEND_URL: "http://localhost:8080",
  BROKER_URL: "ws://localhost:8080/socket"
};
