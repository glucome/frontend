import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { setBodyBackground } from 'app/helpers';
import { AuthenticationService } from 'app/shared/services/authentication.service';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  hidePassword = true;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(["/"]);
    }
  }

  ngOnInit() {
    setBodyBackground();

    this.loginForm = this.formBuilder.group({
      username: new FormControl("", Validators.required),
      password: new FormControl("", Validators.required)
    });

    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  get formControls() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.authenticationService.login(this.formControls.username.value, this.formControls.password.value, this.returnUrl);
  }

  public getUsernameError() {
    if (this.formControls["username"].hasError("required")) {
      return "El nombre de usuario es obligatorio.";
    }
  }

  public getPasswordError() {
    if (this.formControls["password"].hasError("required")) {
      return "La contraseña es obligatoria.";
    }
  }
}
