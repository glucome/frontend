import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgChatModule } from 'ng-chat';
import { NgxPermissionsModule } from 'ngx-permissions';

import { AdminLayoutComponent } from './admin/admin-layout.component';
import { FooterComponent } from './common/footer/footer.component';
import { NavbarComponent } from './common/navbar/navbar.component';
import { PaymentComponent } from './common/notifications/payment/payment.component';
import { RequestItemComponent } from './common/notifications/request-item/request-item.component';
import { SidebarComponent } from './common/sidebar/sidebar.component';
import { LoginComponent } from './login/login.component';
import { ServerErrorComponent } from './server-error/server-error.component';
import { SignupComponent } from './signup/signup.component';

@NgModule({
  imports: [NgxPermissionsModule.forChild(), CommonModule, RouterModule, FormsModule, ReactiveFormsModule, NgChatModule],
  declarations: [
    AdminLayoutComponent,
    FooterComponent,
    LoginComponent,
    NavbarComponent,
    ServerErrorComponent,
    SidebarComponent,
    SignupComponent,
    RequestItemComponent,
    PaymentComponent
  ]
})
export class ComponentsModule {}
