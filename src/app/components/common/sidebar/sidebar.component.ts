import { Component, OnInit } from '@angular/core';

import { Permission } from '../../../enum/Permission';
import { AuthenticationService } from '../../../shared/services/authentication.service';

declare const $: any;

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  PERMISSION_BASIC = Permission.BASIC;
  PERMISSION_PREMIUM = Permission.PREMIUM;

  menuItems: any[];

  constructor(private authenticationService: AuthenticationService) {}

  ngOnInit() {}

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  logout() {
    this.authenticationService.logout();
  }
}
