import { Component, Input, OnInit } from '@angular/core';
import { AlertService } from 'app/shared/services/alert.service';
import { ChatService, RequestPatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

@Component({
  selector: "app-request-item",
  templateUrl: "./request-item.component.html",
  styleUrls: ["./request-item.component.scss"]
})
export class RequestItemComponent implements OnInit {
  constructor(private requestService: RequestPatientService, private chatService: ChatService, private alertService: AlertService) {}

  ngOnInit() {}

  @Input() data;

  accept(idSolicitud) {
    this.requestService
      .acceptRequest(idSolicitud)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Solicitud aceptada", null);
          this.chatService.createChat(this.data.idUsuarioPaciente, this.data.medico.id).subscribe();
          window.location.reload();
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  reject(idSolicitud) {
    this.requestService
      .rejectRequest(idSolicitud)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Solicitd rechazada correctamente", null);
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }
}
