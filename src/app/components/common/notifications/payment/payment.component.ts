import { Component, Input, OnInit } from '@angular/core';

import { PaymentState } from '../../../../enum/PaymentState';

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"]
})
export class PaymentComponent implements OnInit {
  @Input() payment;

  mesage: string;
  colorClass: string;
  iconClass: string;

  constructor() {}

  ngOnInit() {
    switch (this.payment.estado) {
      case PaymentState.APROBADO:
        this.mesage = "El pago ha sido realizado con éxito.";
        this.colorClass = "btn btn-circle btn-sm btn-success";
        this.iconClass = "fas fa-check";
        break;
      case PaymentState.PENDIENTE:
        this.mesage = "Estamos esperando la confirmación del pago.";
        this.colorClass = "btn btn-circle btn-sm btn-warning";
        this.iconClass = "fas fa-exclamation-triangle";
        break;
      case PaymentState.RECHAZADDO:
        this.mesage = "Ha ocurrido un error con el pago.";
        this.colorClass = "btn btn-circle btn-sm btn-danger";
        this.iconClass = "fas fa-times";
        break;
    }
  }
}
