import { Component, OnInit } from '@angular/core';
import { AlertService } from 'app/shared/services/alert.service';
import { AuthenticationService } from 'app/shared/services/authentication.service';
import { RequestPatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { environment } from '../../../../environments/environment';
import { getCurrentUser } from '../../../helpers/storage';
import { PaymentService } from '../../../shared/services/glucome/payment.service';

@Component({
  selector: "app-navbar",
  templateUrl: "./navbar.component.html",
  styleUrls: ["./navbar.component.css"]
})
export class NavbarComponent implements OnInit {
  constructor(
    private authenticationService: AuthenticationService,
    private requestPatientService: RequestPatientService,
    private paymentService: PaymentService,
    private alertService: AlertService
  ) {}

  public requests: Array<any> = [];
  public payments: Array<any> = [];
  public notificationsNumber: number;
  public medicName: string;
  public userId: number;
  public urlBackend: string;
  public apiUrl: string;

  ngOnInit() {
    this.urlBackend = environment.BACKEND_URL;
    this.apiUrl = environment.API_URL;
    let user = getCurrentUser();
    this.medicName = user.asociado.nombre;
    this.userId = user.id;

    this.requestPatientService
      .getLinkingRequest()
      .pipe(first())
      .subscribe(
        data => {
          this.requests = data["respuesta"].solicitudes;
          this.notificationsNumber = this.requests.length;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );

    this.paymentService.getPaymentsUnnotified().subscribe(
      data => {
        this.payments = data["respuesta"].pagos;
        this.notificationsNumber += this.payments.length;
      },
      error => {
        this.alertService.showNotification("error", "Error", error, null);
      }
    );
  }

  markAsNotified() {
    if (this.payments.length == 0) {
      return;
    }

    let paymentsIds: number[] = [];

    for (const payment of this.payments) {
      paymentsIds.push(payment.id);
    }

    this.paymentService.markPaymentsAsNotified(paymentsIds).subscribe();
    this.notificationsNumber = 0;
  }

  logout() {
    this.authenticationService.logout();
  }
}
