import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import { OWL_DATE_TIME_LOCALE, OwlDateTimeIntl, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { ChartsModule } from 'ng2-charts';
import { Ng2CompleterModule } from 'ng2-completer';
import { TooltipModule } from 'ng2-tooltip-directive';
import { NgxPermissionsModule } from 'ngx-permissions';

import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DefaultIntl } from './DefaultIntl';
import { MedicProfileComponent } from './medic-profile/medic-profile.component';
import { PositionComponent } from './medic-profile/position/position.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PatientDetailComponent } from './patient/patient-detail/patient-detail.component';
import { PatientExercisesComponent } from './patient/patient-exercises/patient-exercises.component';
import { PatientFoodComponent } from './patient/patient-food/patient-food.component';
import { PatientHistoryEditComponent } from './patient/patient-history-edit/patient-history-edit.component';
import { PatientHistoryComponent } from './patient/patient-history/patient-history.component';
import { PatientInsulinComponent } from './patient/patient-insulin/patient-insulin.component';
import { PatientListComponent } from './patient/patient-list/patient-list.component';
import { PatientMeasurementComponent } from './patient/patient-measurement/patient-measurement.component';
import { PatientMedicineComponent } from './patient/patient-medicine/patient-medicine.component';
import { PatientNewMeasurementComponent } from './patient/patient-new-measurement/patient-new-measurement.component';
import { PatientComponent } from './patient/patient/patient.component';
import { ReportFoodRankingComponent } from './report/food-ranking/report-food-ranking.component';
import { ReportByAgeRangeComponent } from './report/report-by-age-range/report-by-age-range.component';
import { ReportByDiabetesTypeComponent } from './report/report-by-diabetes-type/report-by-diabetes-type.component';
import { ReportByMeasurementTypeComponent } from './report/report-by-measurement-type/report-by-measurement-type.component';
import { ReportByMomentComponent } from './report/report-by-moment/report-by-moment.component';
import { ReportNumberMeasurementsComponent } from './report/report-number-measurements/report-number-measurements.component';
import { SubscriptionFailureComponent } from './subscription/subscription-failure/subscription-failure.component';
import { SubscriptionPendingComponent } from './subscription/subscription-pending/subscription-pending.component';
import { SubscriptionSuccessComponent } from './subscription/subscription-success/subscription-success.component';
import { SubscriptionComponent } from './subscription/subscription/subscription.component';

export const MY_CUSTOM_FORMATS = {
  parseInput: "DD/MM/YYYY",
  fullPickerInput: "DD/MM/YYYY hh:mm",
  datePickerInput: "DD/MM/YYYY",
  timePickerInput: "hh:mm a",
  monthYearLabel: "MMM-YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM-YYYY"
};
@NgModule({
  imports: [
    GalleryModule,
    RouterModule.forChild(AdminLayoutRoutes),
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    Ng2CompleterModule,
    ChartsModule,
    NgxPermissionsModule.forChild(),
    MatSlideToggleModule
  ],
  declarations: [
    AccessDeniedComponent,
    DashboardComponent,
    NotFoundComponent,
    PatientListComponent,
    PatientDetailComponent,
    PatientMeasurementComponent,
    PatientHistoryComponent,
    PatientHistoryEditComponent,
    MedicProfileComponent,
    PatientMedicineComponent,
    ReportNumberMeasurementsComponent,
    ReportByMomentComponent,
    PatientInsulinComponent,
    ReportByAgeRangeComponent,
    PatientComponent,
    PatientExercisesComponent,
    PatientFoodComponent,
    SubscriptionComponent,
    ReportByDiabetesTypeComponent,
    ReportByMeasurementTypeComponent,
    PatientNewMeasurementComponent,
    SubscriptionSuccessComponent,
    SubscriptionPendingComponent,
    SubscriptionFailureComponent,
    PositionComponent,
    ReportFoodRankingComponent
  ],
  providers: [
    { provide: OWL_DATE_TIME_LOCALE, useValue: MY_CUSTOM_FORMATS },
    {
      provide: OwlDateTimeIntl,
      useClass: DefaultIntl
    }
  ]
})
export class AdminLayoutModule {}
