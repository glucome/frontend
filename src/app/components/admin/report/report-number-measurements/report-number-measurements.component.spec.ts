import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportNumberMeasurementsComponent } from './report-number-measurements.component';

describe("ReportNumberMeasurementsComponent", () => {
  let component: ReportNumberMeasurementsComponent;
  let fixture: ComponentFixture<ReportNumberMeasurementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportNumberMeasurementsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportNumberMeasurementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
