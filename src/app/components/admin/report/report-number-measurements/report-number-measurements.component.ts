import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'app/helpers';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { $enum } from 'ts-enum-util';

import { Sex } from '../../../../enum/SexEnum';
import { getDefaultBarChartOptions } from '../../../../helpers/chart';
import { ReportService } from '../../../../shared/services/glucome/report.service';

@Component({
  selector: "app-report",
  templateUrl: "./report-number-measurements.component.html",
  styleUrls: ["./report-number-measurements.component.css"]
})
export class ReportNumberMeasurementsComponent {
  chartForm: FormGroup;
  submitted = false;
  loading = true;

  private DEFAULT_MIN_MEASUREMENT = 60;
  private DEFAULT_MAX_MEASUREMENT = 100;

  private X_AXE_LABEL = "Medición";
  private Y_AXE_LABEL = "Cantidad de veces registrada";

  public barChartOptions: ChartOptions = getDefaultBarChartOptions(this.X_AXE_LABEL, this.Y_AXE_LABEL);

  public barChartData: ChartDataSets[] = [];
  public barChartLabels: Label[];
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  constructor(private reportService: ReportService, private formBuilder: FormBuilder, private customValidators: CustomValidators) {}

  ngOnInit() {
    this.fillChart(this.DEFAULT_MIN_MEASUREMENT, this.DEFAULT_MAX_MEASUREMENT);

    this.chartForm = this.formBuilder.group(
      {
        minMeasurement: new FormControl(this.DEFAULT_MIN_MEASUREMENT, Validators.compose([Validators.required, Validators.min(0)])),
        maxMeasurement: new FormControl(this.DEFAULT_MAX_MEASUREMENT, Validators.compose([Validators.required, Validators.min(0)]))
      },
      {
        validator: this.customValidators.lessThan("minMeasurement", "maxMeasurement")
      }
    );
  }

  clearChart() {
    this.barChartData = [];
  }

  fillChart(minMeasurement: number, maxMeasurement: number) {
    this.loading = true;
    this.clearChart();

    const interval = maxMeasurement - minMeasurement;

    this.barChartLabels = new Array(interval);
    let j = 0;
    for (let i = minMeasurement; i <= maxMeasurement; i++) {
      this.barChartLabels[j++] = String(i);
    }

    let maleData = new Array(interval);
    let femaleData = new Array(interval);
    let otherData = new Array(interval);

    this.reportService.getReportNumberMeasurements().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];

      reportResponse.map(report => {
        if (report.valor >= minMeasurement && report.valor <= maxMeasurement) {
          const position = report.valor - Number(this.barChartLabels[0]);
          switch (report.sexo) {
            case Sex.Femenino:
              femaleData[position] = report.cantidad;
              break;
            case Sex.Masculino:
              maleData[position] = report.cantidad;
              break;
            case Sex.Otro:
              otherData[position] = report.cantidad;
              break;
          }
        }
      });

      this.barChartData.push({ data: femaleData, label: $enum(Sex).getKeyOrThrow(Sex.Femenino) });
      this.barChartData.push({ data: maleData, label: $enum(Sex).getKeyOrThrow(Sex.Masculino) });
      this.barChartData.push({ data: otherData, label: $enum(Sex).getKeyOrThrow(Sex.Otro) });

      this.loading = false;
    });
  }

  get formControls() {
    return this.chartForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.chartForm.invalid) {
      return;
    }

    this.fillChart(this.formControls["minMeasurement"].value, this.formControls["maxMeasurement"].value);
  }

  public getMinMeasurementError() {
    if (this.formControls["minMeasurement"].hasError("required")) {
      return "El valor mínimo de medición es obligatorio.";
    } else if (this.formControls["minMeasurement"].hasError("min")) {
      return "El valor mínimo de medición no puede ser negativo.";
    }
  }

  public getMaxMeasurementError() {
    if (this.formControls["maxMeasurement"].hasError("required")) {
      return "El valor máximo de medición es obligatorio.";
    } else if (this.formControls["maxMeasurement"].hasError("min")) {
      return "El valor máximo de medición no puede ser negativo.";
    } else if (this.formControls["maxMeasurement"].hasError("lessThan")) {
      return "El valor máximo de medición no puede ser menor al valor mínimo de medición.";
    }
  }
}
