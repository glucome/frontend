import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportFoodRankingComponent } from './report-food-ranking.component';

describe("FoodRankingComponent", () => {
  let component: ReportFoodRankingComponent;
  let fixture: ComponentFixture<ReportFoodRankingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportFoodRankingComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportFoodRankingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
