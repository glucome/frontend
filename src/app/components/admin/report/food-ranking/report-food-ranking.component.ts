import { Component, OnInit } from '@angular/core';
import { DayMoment } from 'app/enum/DayMomentEnum';
import { ReportService } from 'app/shared/services/glucome';
import { $enum } from 'ts-enum-util';

import { environment } from '../../../../../environments/environment';

@Component({
  selector: "app-food-ranking",
  templateUrl: "./report-food-ranking.component.html",
  styleUrls: ["./report-food-ranking.component.css"]
})
export class ReportFoodRankingComponent implements OnInit {
  loading = true;
  apiUrl = environment.API_URL;
  headElements = ["", "Momento", "Nombre", "Descripción", "Cantidad de veces ingerido"];
  dayMomentEnum = $enum(DayMoment);
  foodRanking = [];

  constructor(private reportService: ReportService) {}

  ngOnInit() {
    this.fillTable();
  }

  fillTable() {
    this.loading = true;

    let data = new Array();

    this.reportService.getReportFoodRanking().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];

      let momentoAnterior = "";
      reportResponse.map(report => {
        if (report.momento != momentoAnterior) {
          this.foodRanking.push({
            id: report.id,
            moment: this.dayMomentEnum.getKeyOrThrow(report.momento),
            name: report.nombre,
            description: report.descripcion,
            ingestedTimes: report.vecesIngerido
          });
        }

        momentoAnterior = report.momento;
      });

      this.loading = false;
    });
  }
}
