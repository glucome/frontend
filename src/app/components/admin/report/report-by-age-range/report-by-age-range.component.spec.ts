import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByAgeRangeComponent } from './report-by-age-range.component';

describe('ReportByAgeRangeComponent', () => {
  let component: ReportByAgeRangeComponent;
  let fixture: ComponentFixture<ReportByAgeRangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportByAgeRangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByAgeRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
