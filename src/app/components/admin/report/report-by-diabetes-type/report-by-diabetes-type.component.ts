import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DayMoment } from 'app/enum/DayMomentEnum';
import { Sex } from 'app/enum/SexEnum';
import { CustomValidators } from 'app/helpers';
import { ReportService } from 'app/shared/services/glucome';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { $enum } from 'ts-enum-util';

import { DiabetesType } from '../../../../enum/DiabetesTypeEnum';
import { getDefaultBarChartOptions } from '../../../../helpers/chart';

@Component({
  selector: "app-report-by-diabetes-type",
  templateUrl: "./report-by-diabetes-type.component.html",
  styleUrls: ["./report-by-diabetes-type.component.css"]
})
export class ReportByDiabetesTypeComponent implements OnInit {
  chartForm: FormGroup;
  submitted = false;
  loading = true;

  public diabetesTypeEnum = $enum(DiabetesType).map((value, key) => {
    return { id: value, label: key };
  });
  private DEFAULT_DIABETES_TYPE = this.diabetesTypeEnum[0]["id"];

  private X_AXE_LABEL = "Momento del día";
  private Y_AXE_LABEL = "Medición promedio";

  public barChartOptions: ChartOptions = getDefaultBarChartOptions(this.X_AXE_LABEL, this.Y_AXE_LABEL);

  public barChartData: ChartDataSets[] = [];
  public barChartLabels: Label[] = $enum(DayMoment).getKeys();
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  constructor(private reportService: ReportService, private formBuilder: FormBuilder, private customValidators: CustomValidators) {}

  ngOnInit() {
    this.fillChart(this.DEFAULT_DIABETES_TYPE);

    this.chartForm = this.formBuilder.group({
      diabetesType: [this.DEFAULT_DIABETES_TYPE]
    });
  }

  clearChart() {
    this.barChartData = [];
  }

  fillChart(diabetesType) {
    this.loading = true;
    this.clearChart();
    let femaleData = new Array(this.barChartLabels.length);
    let maleData = new Array(this.barChartLabels.length);
    let otherData = new Array(this.barChartLabels.length);

    this.reportService.getReportByDiabetesType().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];
      reportResponse.map(report => {
        if (report.tipoDiabetes == diabetesType) {
          switch (report.sexo) {
            case Sex.Femenino:
              femaleData.splice(report.momento, 0, report.promedio);
              break;
            case Sex.Masculino:
              maleData.splice(report.momento, 0, report.promedio);
              break;
            case Sex.Otro:
              otherData.splice(report.momento, 0, report.promedio);
              break;
          }
        }
      });

      this.barChartData.push({ data: femaleData, label: $enum(Sex).getKeyOrThrow(Sex.Femenino) });
      this.barChartData.push({ data: maleData, label: $enum(Sex).getKeyOrThrow(Sex.Masculino) });
      this.barChartData.push({ data: otherData, label: $enum(Sex).getKeyOrThrow(Sex.Otro) });

      this.loading = false;
    });
  }

  get formControls() {
    return this.chartForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.chartForm.invalid) {
      return;
    }

    this.fillChart(this.formControls["diabetesType"].value);
  }
}
