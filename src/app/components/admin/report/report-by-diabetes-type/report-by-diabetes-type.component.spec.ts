import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByDiabetesTypeComponent } from './report-by-diabetes-type.component';

describe('ReportByDiabetesTypeComponent', () => {
  let component: ReportByDiabetesTypeComponent;
  let fixture: ComponentFixture<ReportByDiabetesTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportByDiabetesTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByDiabetesTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
