import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByMomentComponent } from './report-by-moment.component';

describe('ReportByMomentComponent', () => {
  let component: ReportByMomentComponent;
  let fixture: ComponentFixture<ReportByMomentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportByMomentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByMomentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
