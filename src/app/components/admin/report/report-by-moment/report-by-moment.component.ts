import { Component, OnInit } from '@angular/core';
import { ReportService } from 'app/shared/services/glucome';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';
import { $enum } from 'ts-enum-util';

import { DayMoment } from '../../../../enum/DayMomentEnum';
import { Sex } from '../../../../enum/SexEnum';
import { getDefaultBarChartOptions } from '../../../../helpers/chart';

@Component({
  selector: "app-report-by-moment",
  templateUrl: "./report-by-moment.component.html",
  styleUrls: ["./report-by-moment.component.scss"]
})
export class ReportByMomentComponent implements OnInit {
  loading = true;

  private X_AXE_LABEL = "Momento del día";
  private Y_AXE_LABEL = "Medición promedio";

  public barChartOptions: ChartOptions = getDefaultBarChartOptions(this.X_AXE_LABEL, this.Y_AXE_LABEL);

  public barChartData: ChartDataSets[] = [];
  public barChartLabels: Label[] = $enum(DayMoment).getKeys();
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  constructor(private reportService: ReportService) {}

  ngOnInit() {
    this.fillChart();
  }

  clearChart() {
    this.barChartData = [];
  }

  fillChart() {
    this.loading = true;
    this.clearChart();

    let maleData = new Array(this.barChartLabels.length);
    let femaleData = new Array(this.barChartLabels.length);
    let otherData = new Array(this.barChartLabels.length);

    this.reportService.getReportByMoment().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];

      reportResponse.map(report => {
        switch (report.sexo) {
          case Sex.Femenino:
            femaleData.splice(report.momento, 0, report.promedio);
            break;
          case Sex.Masculino:
            maleData.splice(report.momento, 0, report.promedio);
            break;
          case Sex.Otro:
            otherData.splice(report.momento, 0, report.promedio);
            break;
        }
      });

      this.barChartData.push({ data: femaleData, label: $enum(Sex).getKeyOrThrow(Sex.Femenino) });
      this.barChartData.push({ data: maleData, label: $enum(Sex).getKeyOrThrow(Sex.Masculino) });
      this.barChartData.push({ data: otherData, label: $enum(Sex).getKeyOrThrow(Sex.Otro) });

      this.loading = false;
    });
  }
}
