import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportByMeasurementTypeComponent } from './report-by-measurement-type.component';

describe('ReportByMeasurementTypeComponent', () => {
  let component: ReportByMeasurementTypeComponent;
  let fixture: ComponentFixture<ReportByMeasurementTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportByMeasurementTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportByMeasurementTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
