import 'rxjs/add/operator/filter';

import { Component } from '@angular/core';
import { RxStompService } from '@stomp/ng2-stompjs';
import { ChatService } from 'app/shared/services/glucome';
import { Message, Theme } from 'ng-chat';

import { getCurrentUser } from '../../helpers/storage';
import { MyChatAdapter } from '../../my-chat-adapter';

@Component({
  selector: "app-admin-layout",
  templateUrl: "./admin-layout.component.html",
  styleUrls: ["./admin-layout.component.css"]
})
export class AdminLayoutComponent {
  userId: number;
  theme = Theme.Light;
  public adapter: MyChatAdapter;

  constructor(private rxStompService: RxStompService, private chatService: ChatService) {
    const user = getCurrentUser();
    this.userId = user.id;
    this.adapter = new MyChatAdapter(rxStompService, chatService);
  }

  markMessagesAsRead(messages: Message[]) {
    this.adapter.markMessagesAsRead(messages);
  }
}
