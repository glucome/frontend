import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { convertDate, getDateFromStringValueDatePicker } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { getCurrentPatient, getCurrentUser } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-history",
  templateUrl: "./patient-history.component.html",
  styleUrls: ["./patient-history.component.scss"]
})
export class PatientHistoryComponent implements OnInit, AfterViewInit {
  patient: any;
  historyItems: any;
  pageNumber: number;
  lastPage: boolean;
  headElements = ["Fecha", "Descripción", "Editar", "Borrar"];

  @ViewChild("fechaDesdeInput", { static: false }) fechaDesdeInput: ElementRef;
  @ViewChild("fechaHastaInput", { static: false }) fechaHastaInput: ElementRef;
  @ViewChild("descripcionInput", { static: false }) descripcionInput: ElementRef;

  constructor(private patientService: PatientService, private alertService: AlertService, private router: Router) {}

  ngAfterViewInit() {
    this.filter();
  }

  ngOnInit() {
    this.patient = getCurrentPatient();
    this.pageNumber = 0;
  }

  back() {
    this.router.navigate(["patient-detail"]);
  }

  filterData() {
    this.pageNumber = 0;
    this.filter();
  }

  filter() {
    let filter = this.getFilters();
    this.patientService
      .getPatientHistory(this.patient.id, filter)
      .pipe(first())
      .subscribe(
        data => {
          this.historyItems = data["respuesta"].itemsHistoria;
          this.lastPage = data["respuesta"].ultimaPagina;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  cleanFilter() {
    this.pageNumber = 0;
    this.fechaDesdeInput.nativeElement.value = "";
    this.fechaHastaInput.nativeElement.value = "";
    this.descripcionInput.nativeElement.value = "";
    this.filter();
  }

  getFilters() {
    let user = getCurrentUser();

    return {
      medico: user.asociado.id,
      fechaDesde: convertDate(getDateFromStringValueDatePicker(this.fechaDesdeInput.nativeElement.value)),
      fechaHasta: convertDate(getDateFromStringValueDatePicker(this.fechaHastaInput.nativeElement.value)),
      descripcion: this.descripcionInput.nativeElement.value,
      numeroPagina: this.pageNumber
    };
  }

  getDescription(description) {
    return description.length > 50 ? description.substring(0, 50) + "..." : description;
  }

  deleteHistory(item) {
    this.patientService
      .deleteHistory(item.id)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Entrada de historia clinica borrada correctamente", null);
          this.filter();
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  editHistory(item) {
    this.router.navigate(["patient-history-edit", item.id]);
  }

  newItem() {
    this.router.navigate(["patient-history-edit"]);
  }

  nextPage() {
    this.pageNumber = +this.pageNumber + 1;
    this.filter();
  }

  previousPage() {
    this.pageNumber = +this.pageNumber - 1;
    this.filter();
  }

  download() {
    let user = getCurrentUser();
    this.patientService.downloadHistory(this.patient.id, user.asociado.id, "Historia - " + this.patient.nombre + ".pdf",this.getFilters());
  }
}
