import { HttpBackend } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DayMoment } from 'app/enum/DayMomentEnum';
import { MeasurementResult } from 'app/enum/MeasurementResultEnum';
import { convertDateTime, getDateTimeFromStringValueDatePicker } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-measurement",
  templateUrl: "./patient-measurement.component.html",
  styleUrls: ["./patient-measurement.component.scss"]
})
export class PatientMeasurementComponent implements OnInit, AfterViewInit {
  patient: any;
  measurements: any;
  pageNumber: number;
  lastPage: boolean;
  moments: any;
  momentSelect;

  @ViewChild("fechaDesdeInput", { static: false }) fechaDesdeInput: ElementRef;
  @ViewChild("fechaHastaInput", { static: false }) fechaHastaInput: ElementRef;
  @ViewChild("valorDesdeInput", { static: false }) valorDesdeInput: ElementRef;
  @ViewChild("valorHastaInput", { static: false }) valorHastaInput: ElementRef;

  constructor(public router: Router, private handler: HttpBackend, private patientService: PatientService, private alertService: AlertService) {}

  ngAfterViewInit() {
    this.filter();
  }

  ngOnInit() {
    this.moments = Object.keys(DayMoment).filter(key => isNaN(Number(DayMoment[key])));
    this.patient = getCurrentPatient();
    this.pageNumber = 0;
    this.momentSelect = null;
  }

  headElements = ["", "Valor", "Momento", "Fecha y Hora"];

  getDayMoment(moment) {
    return DayMoment[moment];
  }

  back() {
    this.router.navigate(["patient-detail"]);
  }

  getClass(result) {
    return MeasurementResult[result];
  }

  filterData() {
    this.pageNumber = 0;
    this.filter();
  }

  filter() {
    let filter = this.getFilters();
    this.patientService
      .getMeasurements(this.patient.id, filter)
      .pipe(first())
      .subscribe(
        data => {
          this.measurements = data["respuesta"].mediciones;
          this.lastPage = data["respuesta"].ultimaPagina;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  cleanFilter() {
    this.pageNumber = 0;
    this.fechaDesdeInput.nativeElement.value = "";
    this.fechaHastaInput.nativeElement.value = "";
    this.valorDesdeInput.nativeElement.value = "";
    this.valorHastaInput.nativeElement.value = "";
    this.momentSelect = null;
    this.filter();
  }

  nextPage() {
    this.pageNumber = +this.pageNumber + 1;
    this.filter();
  }

  previousPage() {
    this.pageNumber = +this.pageNumber - 1;
    this.filter();
  }

  getFilters() {
    return {
      fechaHoraDesde: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaDesdeInput.nativeElement.value)),
      fechaHoraHasta: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaHastaInput.nativeElement.value)),
      valorDesde: this.valorDesdeInput.nativeElement.value,
      valorHasta: this.valorHastaInput.nativeElement.value,
      momento: this.momentSelect != null ? this.momentSelect : "",
      numeroPagina: this.pageNumber
    };
  }

  download() {
    this.patientService.downloadMeasurements(this.patient.id, "Mediciones - " + this.patient.nombre + ".xls", this.getFilters());
  }
}
