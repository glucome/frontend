import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InsulinType } from 'app/enum/InsulinType';
import { CustomValidators } from 'app/helpers';
import { convertDateTime, getDateTimeFromStringValueDatePicker } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-insulin",
  templateUrl: "./patient-insulin.component.html",
  styleUrls: ["./patient-insulin.component.scss"]
})
export class PatientInsulinComponent implements OnInit, AfterViewInit {
  patient: any;
  insulins: any;
  pageNumber: number;
  lastPage: boolean;
  types: any;
  typeSelect;
  insulinFactorForm: any;

  @ViewChild("fechaDesdeInput", { static: false }) fechaDesdeInput: ElementRef;
  @ViewChild("fechaHastaInput", { static: false }) fechaHastaInput: ElementRef;
  @ViewChild("unidadesDesdeInput", { static: false }) unidadesDesdeInput: ElementRef;
  @ViewChild("unidadesHastaInput", { static: false }) unidadesHastaInput: ElementRef;
  submitted: boolean;
  carbohydrate: any;
  sugar: any;
  insulineFactor = false;

  constructor(
    private formBuilder: FormBuilder,
    private customValidators: CustomValidators,
    public router: Router,
    private patientService: PatientService,
    private alertService: AlertService
  ) {}

  ngAfterViewInit() {
    this.filter();
  }

  ngOnInit() {
    this.types = Object.keys(InsulinType).filter(key => isNaN(Number(InsulinType[key])));
    this.patient = getCurrentPatient();
    this.pageNumber = 0;
    this.typeSelect = null;

    this.insulinFactorForm = this.formBuilder.group({
      sugar: new FormControl("", Validators.compose([Validators.required, this.customValidators.notNumber("sugar")])),
      carbohydrates: new FormControl("", Validators.compose([Validators.required, this.customValidators.notNumber("carbohydrates")]))
    });

    this.patientService
      .getInsulinFactor(this.patient.id)
      .pipe(first())
      .subscribe(
        data => {
          this.formControls["sugar"].setValue(data["respuesta"].factorCorreccionAzucar);
          this.formControls["carbohydrates"].setValue(data["respuesta"].factorCorreccionCHO);
          this.insulineFactor = true;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  headElements = ["Unidades", "Tipo", "Fecha y Hora"];

  getInsulinTypes(type) {
    return InsulinType[type];
  }

  back() {
    this.router.navigate(["patient-detail"]);
  }

  filterData() {
    this.pageNumber = 0;
    this.filter();
  }

  filter() {
    let filter = this.getFilters();
    this.patientService
      .getInjectedInsulin(this.patient.id, filter)
      .pipe(first())
      .subscribe(
        data => {
          this.insulins = data["respuesta"].insulinaInyectadas;
          this.lastPage = data["respuesta"].ultimaPagina;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  cleanFilter() {
    this.pageNumber = 0;
    this.fechaDesdeInput.nativeElement.value = "";
    this.fechaHastaInput.nativeElement.value = "";
    this.unidadesDesdeInput.nativeElement.value = "";
    this.unidadesHastaInput.nativeElement.value = "";
    this.typeSelect = null;
    this.filter();
  }

  nextPage() {
    this.pageNumber = +this.pageNumber + 1;
    this.filter();
  }

  previousPage() {
    this.pageNumber = +this.pageNumber - 1;
    this.filter();
  }

  getFilters() {
    return {
      fechaHoraDesde: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaDesdeInput.nativeElement.value)),
      fechaHoraHasta: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaHastaInput.nativeElement.value)),
      unidadesDesde: this.unidadesDesdeInput.nativeElement.value,
      unidadesHasta: this.unidadesHastaInput.nativeElement.value,
      tipo: this.typeSelect != null ? this.typeSelect : "",
      numeroPagina: this.pageNumber
    };
  }

  get formControls() {
    return this.insulinFactorForm.controls;
  }

  getSugarError() {
    if (this.formControls["sugar"].hasError("required")) {
      return "El factor de corrección del azúcar es obligatorio.";
    } else if (this.formControls["sugar"].hasError("notNumber")) {
      return "El factor de corrección del azúcar debe ser un número válido";
    }
  }

  getHidratosError() {
    if (this.formControls["carbohydrates"].hasError("required")) {
      return "El factor de corrección de hidratos de carbono es obligatorio.";
    } else if (this.formControls["carbohydrates"].hasError("notNumber")) {
      return "El factor de corrección de hidratos de carbono debe ser un número válido";
    }
  }

  public onInsulinFactorSubmit() {
    this.submitted = true;

    if (this.insulinFactorForm.invalid) {
      return;
    }

    this.patientService
      .newInsulineFactor(this.patient.id, this.formControls.sugar.value, this.formControls.carbohydrates.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Factores de insulina grabados correctamente", null);
          jQuery("#insulinFactor")["modal"]("hide");
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }
}
