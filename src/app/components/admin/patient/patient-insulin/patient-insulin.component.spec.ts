import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientInsulinComponent } from './patient-insulin.component';

describe('PatientInsulinComponent', () => {
  let component: PatientInsulinComponent;
  let fixture: ComponentFixture<PatientInsulinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientInsulinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientInsulinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
