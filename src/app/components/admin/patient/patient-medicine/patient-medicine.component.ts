import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { CompleterItem } from 'ng2-completer';
import { first } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';
import { CustomData } from './custom-data';

@Component({
  selector: "app-patient-medicine",
  templateUrl: "./patient-medicine.component.html",
  styleUrls: ["./patient-medicine.component.scss"]
})
export class PatientMedicineComponent implements OnInit {
  patient: any;
  medicines: any;
  pageNumber: number;
  lastPage: boolean;
  headElements = ["Droga", "Marca", "Presentación", "Laboratorio"];
  public medicine: any;
  public customData: CustomData;
  public selectedMedicine: CompleterItem;
  public submitted: boolean;
  public isInvalid: boolean;

  constructor(private patientService: PatientService, private alertService: AlertService, private router: Router) {
    this.customData = new CustomData(patientService);
  }

  public onMedicineSelected(selected: CompleterItem) {
    this.selectedMedicine = selected;
  }

  ngOnInit() {
    this.patient = getCurrentPatient();
    this.pageNumber = 0;
    this.patientService
      .getPatientMedicine(this.patient.id)
      .pipe(first())
      .subscribe(
        data => {
          this.medicines = data["respuesta"].medicamentos;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  save() {
    this.submitted = true;
    this.isInvalid = !this.selectedMedicine;

    if (this.isInvalid) {
      return;
    }

    this.patientService
      .savePatientMedicine(this.getBody())
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Medicamento guardado correctamente.", null);
          window.location.reload();
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  getBody() {
    return {
      idPaciente: this.patient.id,
      idMedicamento: this.selectedMedicine.originalObject.id
    };
  }

  getMedicineError() {
    return "El medicamento es obligatorio";
  }

  back() {
    this.router.navigate(["patient-detail"]);
  }
}
