import { PatientService } from 'app/shared/services/glucome';
import { CompleterData, CompleterItem } from 'ng2-completer';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';

export class CustomData extends Subject<CompleterItem[]> implements CompleterData {
  patient: any;
  constructor(private patientService: PatientService) {
    super();
  }
  public search(term: string): void {
    this.patient = getCurrentPatient();
    this.patientService
      .getMedicines(this.patient.id, term)
      .pipe(
        map((data: any) => {
          const matches = data.respuesta.medicamentos
            .map((episode: any) => this.convertToItem(episode))
            .filter((episode: CompleterItem | null) => !!episode) as CompleterItem[];
          this.next(matches);
        })
      )
      .subscribe();
  }

  public cancel() {
    // Handle cancel
  }

  public convertToItem(data: any): CompleterItem | null {
    if (!data) {
      return null;
    }
    return {
      title: data.droga + " - " + data.laboratorio + " - " + data.marca + " - " + data.presentacion,
      originalObject: data
    } as CompleterItem;
  }
}
