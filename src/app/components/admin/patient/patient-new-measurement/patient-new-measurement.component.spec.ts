import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientNewMeasurementComponent } from './patient-new-measurement.component';

describe('PatientNewMeasurementComponent', () => {
  let component: PatientNewMeasurementComponent;
  let fixture: ComponentFixture<PatientNewMeasurementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientNewMeasurementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientNewMeasurementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
