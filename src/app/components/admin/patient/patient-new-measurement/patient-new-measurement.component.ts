import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DayMoment } from 'app/enum/DayMomentEnum';
import { MeasurementType } from 'app/enum/MeasurementTypeEnum';
import { CustomValidators } from 'app/helpers';
import { convertDateTime } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-new-measurement",
  templateUrl: "./patient-new-measurement.component.html",
  styleUrls: ["./patient-new-measurement.component.scss"]
})
export class PatientNewMeasurementComponent implements OnInit {
  moments: string[];
  patient: any;
  types: string[];
  form: FormGroup;
  submitted: boolean;
  momentSelect;
  typeSelect;

  constructor(
    private formBuilder: FormBuilder,
    private patientService: PatientService,
    private router: Router,
    private customValidators: CustomValidators,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.moments = Object.keys(DayMoment).filter(key => isNaN(Number(DayMoment[key])));
    this.types = Object.keys(MeasurementType).filter(key => isNaN(Number(MeasurementType[key])));
    this.patient = getCurrentPatient();
    this.form = this.formBuilder.group({
      datetime: new FormControl(new Date(), Validators.required),
      value: new FormControl("", Validators.compose([Validators.required, this.customValidators.notNumber("value")])),
      moment: new FormControl(0, Validators.required),
      type: new FormControl(0, Validators.required)
    });
  }

  get formControls() {
    return this.form.controls;
  }

  getDayMoment(moment) {
    return DayMoment[moment];
  }

  getType(type) {
    return MeasurementType[type];
  }

  public getDateTimeError() {
    if (this.formControls["datetime"].hasError("required")) {
      return "La fecha y hora son obligatorias.";
    }
  }

  public getValueError() {
    if (this.formControls["value"].hasError("required")) {
      return "El valor es obligatorio.";
    } else if (this.formControls["value"].hasError("notNumber")) {
      return "El valor debe ser un número válido";
    }
  }

  public getMomentError() {
    if (this.formControls["moment"].hasError("required")) {
      return "El momento es obligatorio";
    }
  }

  public getTypeError() {
    if (this.formControls["type"].hasError("required")) {
      return "El tipo es obligatorio";
    }
  }

  public onSubmit() {
    this.submitted = true;

    if (this.form.invalid) {
      return;
    }

    this.patientService
      .newMeasurement(
        this.patient.id,
        convertDateTime(this.formControls.datetime.value),
        this.formControls.value.value,
        this.formControls.moment.value,
        this.formControls.type.value
      )
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Medición grabada correctamente", null);
          this.back();
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  back() {
    this.router.navigate(["/patient-measurement"]);
  }
}
