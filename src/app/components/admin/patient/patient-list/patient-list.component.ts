import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';

import { DiabetesType } from '../../../../enum/DiabetesTypeEnum';
import { Permission } from '../../../../enum/Permission';
import { Role } from '../../../../enum/Role';
import { setCurrentPatient } from '../../../../helpers/storage';
import { AlertService } from '../../../../shared/services/alert.service';
import { PatientService } from '../../../../shared/services/glucome';
import { environment } from '../.././../../../environments/environment';

@Component({
  selector: "app-patient-list",
  templateUrl: "./patient-list.component.html",
  styleUrls: ["./patient-list.component.scss"]
})
export class PatientListComponent implements OnInit {
  PERMISSION_BASIC = Permission.BASIC;
  PERMISSION_PREMIUM = Permission.PREMIUM;

  public patients: Array<any> = [];
  urlBackend: any;
  apiUrl: any;
  headElements = ["", "Nombre", "Fecha Nacimiento", "Tipo de diabetes", "Acciones"];

  constructor(private patientService: PatientService, private alertService: AlertService, private router: Router) {}

  ngOnInit() {
    Role;
    this.urlBackend = environment.BACKEND_URL;
    this.apiUrl = environment.API_URL;
    this.patientService
      .getPatients()
      .pipe(first())
      .subscribe(
        data => {
          this.patients = data["respuesta"].pacientes;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  getDiabetesType(type) {
    return DiabetesType[type];
  }

  goToPatient(patient) {
    setCurrentPatient(patient);
    this.router.navigate(["patient-detail"]);
  }

  newPatient() {
    this.router.navigate(["patient", 0]);
  }
}
