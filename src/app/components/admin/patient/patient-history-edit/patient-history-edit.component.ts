import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { convertDate } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { getCurrentPatient, getCurrentUser } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-history-edit",
  templateUrl: "./patient-history-edit.component.html",
  styleUrls: ["./patient-history-edit.component.scss"]
})
export class PatientHistoryEditComponent implements OnInit {
  patient: any;
  medic: any;
  historyForm: FormGroup;
  submitted = false;
  returnUrl: string;
  item: any;
  isEdit: boolean;
  id: any;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private patientService: PatientService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.patient = getCurrentPatient();
    this.medic = getCurrentUser().asociado;

    this.route.params.subscribe(params => {
      this.id = params["id"];
    });

    if (this.id != undefined) {
      this.patientService
        .getItemHistory(this.id)
        .pipe(first())
        .subscribe(
          data => {
            this.item = data["respuesta"].item;
            let dateSplit = this.item.fecha.split("/");
            this.historyForm = this.formBuilder.group({
              date: new FormControl(new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0], 0, 0, 0, 0), Validators.required),
              description: new FormControl(this.item.descripcion, Validators.required),
              id: new FormControl(this.item.id, Validators.required)
            });

            this.isEdit = true;
          },
          error => {
            this.alertService.showNotification("error", "Error", error, null);
            this.router.navigate([this.returnUrl]);
          }
        );
    } else {
      this.item = { fecha: convertDate(new Date()), descripcion: "", id: 0 };
      let dateSplit = this.item.fecha.split("/");
      this.historyForm = this.formBuilder.group({
        date: new FormControl(new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0], 0, 0, 0, 0), Validators.required),
        description: new FormControl(this.item.descripcion, Validators.required),
        id: new FormControl(this.item.id, Validators.required)
      });

      this.isEdit = false;
    }

    this.returnUrl = "patient-history";
  }

  get formControls() {
    return this.historyForm.controls;
  }

  back() {
    this.router.navigate(["patient-history"]);
  }

  save() {
    this.submitted = true;

    if (this.historyForm.invalid) {
      return;
    }

    this.patientService
      .saveItemHistory(this.patient.id, this.getBody(), this.isEdit)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.showNotification("success", "Éxito", "Entrada de historia grabada correctamente ", null);
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  getBody() {
    return {
      id: this.formControls.id.value,
      fecha: convertDate(this.formControls.date.value),
      descripcion: this.formControls.description.value,
      medico: this.medic.id
    };
  }

  public getDateError() {
    if (this.formControls["date"].hasError("required")) {
      return "La fecha es obligatoria.";
    }
  }

  public getDescriptionError() {
    if (this.formControls["description"].hasError("required")) {
      return "La descripción es obligatoria.";
    }
  }
}
