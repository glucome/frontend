import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientHistoryEditComponent } from './patient-history-edit.component';

describe("PatientHistoryEditComponent", () => {
  let component: PatientHistoryEditComponent;
  let fixture: ComponentFixture<PatientHistoryEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PatientHistoryEditComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientHistoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
