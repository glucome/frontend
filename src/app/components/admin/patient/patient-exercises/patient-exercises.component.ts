import { HttpBackend } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ExcersiseType } from 'app/enum/ExcersiseType';
import { convertDateTime, getDateTimeFromStringValueDatePicker } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-exercises",
  templateUrl: "./patient-exercises.component.html",
  styleUrls: ["./patient-exercises.component.scss"]
})
export class PatientExercisesComponent implements OnInit, AfterViewInit {
  patient: any;
  excersises: any;
  pageNumber: number;
  lastPage: boolean;
  types: any;
  typeSelect;

  @ViewChild("fechaDesdeInput", { static: false }) fechaDesdeInput: ElementRef;
  @ViewChild("fechaHastaInput", { static: false }) fechaHastaInput: ElementRef;
  @ViewChild("minutosDesdeInput", { static: false }) minutosDesdeInput: ElementRef;
  @ViewChild("minutosHastaInput", { static: false }) minutosHastaInput: ElementRef;

  constructor(public router: Router, private handler: HttpBackend, private patientService: PatientService, private alertService: AlertService) {}

  ngAfterViewInit() {
    this.filter();
  }

  ngOnInit() {
    this.types = Object.keys(ExcersiseType).filter(key => isNaN(Number(ExcersiseType[key])));
    this.patient = getCurrentPatient();
    this.typeSelect = null;
    this.pageNumber = 0;
    this.typeSelect = null;
  }

  headElements = ["Minutos", "Tipo", "Fecha y Hora"];

  public getExercisesTypes(type) {
    return ExcersiseType[type];
  }

  back() {
    this.router.navigate(["patient-detail"]);
  }

  filterData() {
    this.pageNumber = 0;
    this.filter();
  }

  filter() {
    let filter = this.getFilters();
    this.patientService
      .getExercises(this.patient.id, filter)
      .pipe(first())
      .subscribe(
        data => {
          this.excersises = data["respuesta"].ejercicios;
          this.lastPage = data["respuesta"].ultimaPagina;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  cleanFilter() {
    this.pageNumber = 0;
    this.fechaDesdeInput.nativeElement.value = "";
    this.fechaHastaInput.nativeElement.value = "";
    this.minutosDesdeInput.nativeElement.value = "";
    this.minutosHastaInput.nativeElement.value = "";
    this.typeSelect = null;
    this.filter();
  }

  nextPage() {
    this.pageNumber = +this.pageNumber + 1;
    this.filter();
  }

  previousPage() {
    this.pageNumber = +this.pageNumber - 1;
    this.filter();
  }

  getFilters() {
    return {
      fechaHoraDesde: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaDesdeInput.nativeElement.value)),
      fechaHoraHasta: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaHastaInput.nativeElement.value)),
      minutosDesde: this.minutosDesdeInput.nativeElement.value,
      minutosHasta: this.minutosHastaInput.nativeElement.value,
      tipo: this.typeSelect != null ? this.typeSelect : "",
      numeroPagina: this.pageNumber
    };
  }

  /* download() {
    this.patientService.downloadMeasurements(this.patient.id, "Mediciones - " + this.patient.nombre + ".xls", this.getFilters());
  }*/
}
