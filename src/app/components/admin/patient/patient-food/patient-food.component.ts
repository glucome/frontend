import { HttpBackend } from '@angular/common/http';
import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DescriptionStrategy, Image } from '@ks89/angular-modal-gallery';
import { FoodMoment } from 'app/enum/FoodMoment';
import { convertDateTime, getDateTimeFromStringValueDatePicker } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { environment } from 'environments/environment';
import { first } from 'rxjs/operators';

import { getCurrentPatient } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-food",
  templateUrl: "./patient-food.component.html",
  styleUrls: ["./patient-food.component.scss"]
})
export class PatientFoodComponent implements OnInit, AfterViewInit {
  apiUrl: string;
  patient: any;
  foods: any;
  pageNumber: number;
  lastPage: boolean;
  types: any;
  typeSelect;

  @ViewChild("fechaDesdeInput", { static: false }) fechaDesdeInput: ElementRef;
  @ViewChild("fechaHastaInput", { static: false }) fechaHastaInput: ElementRef;
  @ViewChild("porcionesDesdeInput", { static: false }) porcionesDesdeInput: ElementRef;
  @ViewChild("porcionesHastaInput", { static: false }) porcionesHastaInput: ElementRef;

  constructor(public router: Router, private handler: HttpBackend, private patientService: PatientService, private alertService: AlertService) {}

  ngAfterViewInit() {
    this.filter();
  }

  ngOnInit() {
    this.apiUrl = environment.API_URL;
    this.types = Object.keys(FoodMoment).filter(key => isNaN(Number(FoodMoment[key])));
    this.patient = getCurrentPatient();

    this.pageNumber = 0;
    this.typeSelect = null;
  }

  headElements = ["", "Nombre", "Índice glucémico", "Momento", "Porcion de referencia", "Porciones", "Fecha y Hora"];

  public getFoodMoment(type) {
    return FoodMoment[type];
  }

  back() {
    this.router.navigate(["patient-detail"]);
  }

  filterData() {
    this.pageNumber = 0;
    this.filter();
  }
  filter() {
    let filter = this.getFilters();
    this.patientService
      .getPatientFoods(this.patient.id, filter)
      .pipe(first())
      .subscribe(
        data => {
          this.foods = data["respuesta"].alimentoIngerido;
          this.lastPage = data["respuesta"].ultimaPagina;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  cleanFilter() {
    this.pageNumber = 0;
    this.fechaDesdeInput.nativeElement.value = "";
    this.fechaHastaInput.nativeElement.value = "";
    this.porcionesDesdeInput.nativeElement.value = "";
    this.porcionesHastaInput.nativeElement.value = "";
    this.typeSelect = null;
    this.filter();
  }

  nextPage() {
    this.pageNumber = +this.pageNumber + 1;
    this.filter();
  }

  previousPage() {
    this.pageNumber = +this.pageNumber - 1;
    this.filter();
  }

  getFilters() {
    return {
      fechaHoraDesde: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaDesdeInput.nativeElement.value)),
      fechaHoraHasta: convertDateTime(getDateTimeFromStringValueDatePicker(this.fechaHastaInput.nativeElement.value)),
      porcionesDesde: this.porcionesDesdeInput.nativeElement.value,
      porcionesHasta: this.porcionesHastaInput.nativeElement.value,
      momento: this.typeSelect != null ? this.typeSelect : "",
      numeroPagina: this.pageNumber
    };
  }

  getImage(food) {
    return food.imagen != null && food.imagen != undefined ? food.imagen : food.alimento.id;
  }

  getImageGallery(food) {
    return [
      new Image(
        0,
        {
          img: `${this.apiUrl}/imagen/alimento_ingerido/${this.getImage(food)}/`
        },
        {
          img: `${this.apiUrl}/imagen/alimento_ingerido/${this.getImage(food)}/`
        }
      )
    ];
  }

  getDescriptionConfig(food) {
    const customDescription = {
      strategy: DescriptionStrategy.ALWAYS_VISIBLE,
      customFullDescription:
        food.alimento.nombre + (food.observacion != null && food.observacion != undefined && food.observacion != "" ? ": " + food.observacion : "")
    };
    return customDescription;
  }
}
