import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientFoodComponent } from './patient-food.component';

describe('PatientFoodComponent', () => {
  let component: PatientFoodComponent;
  let fixture: ComponentFixture<PatientFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
