import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DayMoment } from 'app/enum/DayMomentEnum';
import { DiabetesType } from 'app/enum/DiabetesTypeEnum';
import { MeasurementResult } from 'app/enum/MeasurementResultEnum';
import { Sex } from 'app/enum/SexEnum';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { environment } from 'environments/environment';
import { Label } from 'ng2-charts';
import { first } from 'rxjs/operators';

import { getDefaultBarChartOptions } from '../../../../helpers/chart';
import { getCurrentPatient } from '../../../../helpers/storage';

@Component({
  selector: "app-patient-detail",
  templateUrl: "./patient-detail.component.html",
  styleUrls: ["./patient-detail.component.scss"]
})
export class PatientDetailComponent implements OnInit {
  public patient;
  public lastMeasurement;
  apiUrl: any;
  urlBackend: any;
  glycosylatedHemoglobin: any;
  loadingChartLatestMeasurements = true;
  loadingChartGlycosylatedHemoglobinLastYear = true;

  public barChartLatestMeasurementsOptions: ChartOptions = getDefaultBarChartOptions();
  public barChartGlycosylatedHemoglobinLastYearOptions: ChartOptions = getDefaultBarChartOptions(null, null, "%");

  public barChartLatestMeasurementsData: ChartDataSets[] = [];
  public barChartGlycosylatedHemoglobinLastYearData: ChartDataSets[] = [];

  public barLatestMeasurementsLabels: Label[] = [];
  public barChartGlycosylatedHemoglobinLastYearLabels: Label[] = [];

  public barChartType: ChartType = "line";
  public barChartLegend = false;
  public barChartPlugins = [pluginDataLabels];

  constructor(public router: Router, private patientService: PatientService, private alertService: AlertService, location: Location) {}

  async ngOnInit() {
    this.patient = await getCurrentPatient();
    this.urlBackend = environment.BACKEND_URL;
    this.apiUrl = environment.API_URL;

    this.patientService
      .getLastMeasurement(this.patient.id)
      .pipe(first())
      .subscribe(
        data => {
          this.lastMeasurement = data["respuesta"].medicion;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );

    this.patientService
      .getGlycosylatedHemoglobin(this.patient.id)
      .pipe(first())
      .subscribe(
        data => {
          this.glycosylatedHemoglobin = data["respuesta"].hemoglobinaGlicosilada;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );

    this.fillChartLatestMeasurements();
    this.fillChartGlycosylatedHemoglobinLastYear();
  }

  getDiabetesType(type) {
    return DiabetesType[type];
  }

  getSex(sex) {
    return Sex[sex];
  }

  getDayMoment(moment) {
    return DayMoment[moment];
  }

  back() {
    this.router.navigate(["patients"]);
  }

  getClass(result) {
    return MeasurementResult[result];
  }

  clearChartLatestMeasurements() {
    this.barChartLatestMeasurementsData = [];
  }

  clearChartGlycosylatedHemoglobinLastYear() {
    this.barChartGlycosylatedHemoglobinLastYearData = [];
  }

  fillChartLatestMeasurements() {
    this.loadingChartLatestMeasurements = true;
    this.clearChartLatestMeasurements();

    let data = [];

    this.patientService.getLatestMeasurements(this.patient.id).subscribe(resp => {
      const measurementsResponse = resp["respuesta"]["mediciones"];

      measurementsResponse.map(measurement => {
        this.barLatestMeasurementsLabels.push(measurement.fechaHora);
        data.push(measurement.valor);
      });
    });

    this.barChartLatestMeasurementsData.push({ data: data, label: "" });

    this.loadingChartLatestMeasurements = false;
  }

  fillChartGlycosylatedHemoglobinLastYear() {
    this.loadingChartGlycosylatedHemoglobinLastYear = true;
    this.clearChartGlycosylatedHemoglobinLastYear();

    let data = [];

    this.patientService.getGlycosylatedHemoglobinYearly(this.patient.id).subscribe(resp => {
      const glycosylatedHemoglobinResponse = resp["respuesta"]["hemoglobinaGlicosiladaAnual"];
      const currentTrimester = glycosylatedHemoglobinResponse.trimestreActual;
      delete glycosylatedHemoglobinResponse.trimestreActual;

      this.barChartGlycosylatedHemoglobinLastYearLabels = Object.keys(glycosylatedHemoglobinResponse).map(months => {
        return months
          .toString()
          .split(/(?=[A-Z])/)
          .map(month => {
            return month.charAt(0).toUpperCase() + month.slice(1);
          })
          .join("/");
      });

      this.barChartGlycosylatedHemoglobinLastYearLabels[currentTrimester] += " (Actual)";

      Object.keys(glycosylatedHemoglobinResponse).forEach((key, i) => {
        data.push(glycosylatedHemoglobinResponse[key]);
      });
    });

    this.barChartGlycosylatedHemoglobinLastYearData.push({ data: data, label: "" });

    this.loadingChartGlycosylatedHemoglobinLastYear = false;
  }
}
