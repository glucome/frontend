import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DiabetesType } from 'app/enum/DiabetesTypeEnum';
import { Sex } from 'app/enum/SexEnum';
import { CustomValidators } from 'app/helpers';
import { convertDate } from 'app/helpers/date-helper';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { $enum } from 'ts-enum-util';

import { getCurrentUser } from '../../../../helpers/storage';

@Component({
  selector: "app-patient",
  templateUrl: "./patient.component.html",
  styleUrls: ["./patient.component.scss"]
})
export class PatientComponent implements OnInit {
  id;
  patientForm: FormGroup;
  submitted = false;
  loading = true;

  public sexEnum = $enum(Sex).map((value, key) => {
    return { id: value, label: key };
  });
  private DEFAULT_SEX = this.sexEnum[0]["id"];

  public diabetesTypeEnum = $enum(DiabetesType).map((value, key) => {
    return { id: value, label: key };
  });
  private DEFAULT_DIABETES_TYPE = this.diabetesTypeEnum[0]["id"];

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private patientService: PatientService,
    private customValidators: CustomValidators,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params["id"];
    });

    let dateSplit = convertDate(new Date()).split("/");

    this.patientForm = this.formBuilder.group({
      id: new FormControl(this.id, Validators.required),
      name: new FormControl("", Validators.required),
      height: new FormControl("", Validators.compose([Validators.required, this.customValidators.notNumber("height")])),
      weight: new FormControl("", Validators.compose([Validators.required, this.customValidators.notNumber("weight")])),
      birthDate: new FormControl(new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0], 0, 0, 0, 0), Validators.required),
      sex: [this.DEFAULT_SEX],
      diabetesType: [this.DEFAULT_DIABETES_TYPE]
    });
  }

  get formControls() {
    return this.patientForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.patientForm.invalid) {
      return;
    }

    const user = getCurrentUser();
    const body = {
      id: this.formControls.id.value,
      nombre: this.formControls.name.value,
      altura: this.formControls.height.value.replace(",", "."),
      peso: this.formControls.weight.value.replace(",", "."),
      fechaNacimiento: convertDate(this.formControls.birthDate.value),
      sexo: this.formControls.sex.value,
      tipoDiabetes: this.formControls.diabetesType.value
    };

    this.patientService.registerExternalPatient(user.asociado.id, body).subscribe(
      data => {
        this.alertService.showNotification("success", "Éxito", "Paciente registrado con éxito", null);
        this.router.navigate(["/patients"]);
      },
      error => {
        this.alertService.showNotification("error", "Error", error, null);
      }
    );
  }

  public getIDError() {
    if (this.formControls["id"].hasError("required")) {
      return "El ID es obligatorio.";
    }
  }

  public getNameError() {
    if (this.formControls["name"].hasError("required")) {
      return "El nombre es obligatorio.";
    }
  }

  public getHeightError() {
    if (this.formControls["height"].hasError("required")) {
      return "La altura es obligatoria.";
    } else if (this.formControls["height"].hasError("notNumber")) {
      return "Ingrese una altura válida.";
    }
  }

  public getWeightError() {
    if (this.formControls["weight"].hasError("required")) {
      return "El peso es obligatorio.";
    } else if (this.formControls["weight"].hasError("notNumber")) {
      return "Ingrese un peso válido.";
    }
  }

  public getBirthDateError() {
    if (this.formControls["birthDate"].hasError("required")) {
      return "La fecha de nacimiento es obligatoria.";
    }
  }
}
