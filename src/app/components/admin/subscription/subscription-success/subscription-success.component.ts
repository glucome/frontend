import { Component, OnInit } from '@angular/core';

import { getCurrentUser, setCurrentUser } from '../../../../helpers/storage';
import { AuthenticationService } from '../../../../shared/services/authentication.service';
import { MedicService } from '../../../../shared/services/glucome/medic.service';

@Component({
  selector: "app-subscription-success",
  templateUrl: "./subscription-success.component.html",
  styleUrls: ["./subscription-success.component.css"]
})
export class SubscriptionSuccessComponent implements OnInit {
  function: String;

  constructor(private medicService: MedicService, private authenticationService: AuthenticationService) {}

  ngOnInit() {
    this.medicService.getRoles().subscribe(
      res => {
        const roles = res["respuesta"]["roles"];
        let currentUser = getCurrentUser();
        currentUser.roles = roles;
        setCurrentUser(currentUser);
        this.function = "reload";
      },
      error => {
        this.function = "login";
      }
    );
  }

  reload() {
    window.location.reload();
  }

  login() {
    this.authenticationService.logout();
  }
}
