import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionPendingComponent } from './subscription-pending.component';

describe('SubscriptionPendingComponent', () => {
  let component: SubscriptionPendingComponent;
  let fixture: ComponentFixture<SubscriptionPendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionPendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionPendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
