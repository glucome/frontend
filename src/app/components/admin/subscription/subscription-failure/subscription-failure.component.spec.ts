import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionFailureComponent } from './subscription-failure.component';

describe('SubscriptionFailureComponent', () => {
  let component: SubscriptionFailureComponent;
  let fixture: ComponentFixture<SubscriptionFailureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubscriptionFailureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionFailureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
