import { Component, OnInit } from '@angular/core';

import { Plan } from '../../../../enum/Plan';
import { isPremium } from '../../../../helpers/permissions';
import { AlertService } from '../../../../shared/services/alert.service';
import { AuthenticationService } from '../../../../shared/services/authentication.service';
import { MercadoPagoService } from '../../../../shared/services/glucome/mercado-pago.service';
import { SubscriptionService } from '../../../../shared/services/glucome/subscription.service';

@Component({
  selector: "app-subscription",
  templateUrl: "./subscription.component.html",
  styleUrls: ["./subscription.component.css"]
})
export class SubscriptionComponent implements OnInit {
  MEDIC_BASIC = Plan.MEDIC_BASIC;
  MEDIC_PREMIUM = Plan.MEDIC_PREMIUM;
  initPoint: string;
  isPremium: boolean;

  constructor(
    private subscriptionService: SubscriptionService,
    private authenticationService: AuthenticationService,
    private alertService: AlertService,
    private mercadoPagoService: MercadoPagoService
  ) {}

  ngOnInit() {
    if (isPremium()) {
      this.isPremium = true;
      return;
    }

    this.mercadoPagoService.getInitPoint().subscribe(
      res => {
        this.initPoint = res["respuesta"].initPoint;
      },
      error => {
        this.alertService.showNotification("error", "Éxito", "Ha ocurrido un error al configurar MercadoPago", null);
      }
    );
  }

  activateSubscription(planId: number) {
    this.subscriptionService.activateSubscription(planId).subscribe(
      resp => {
        this.alertService.showNotification("success", "Éxito", "Se ha cambiado la suscripción exitosamente", null);
        this.authenticationService.logout();
      },
      error => {
        this.alertService.showNotification("error", "Error", error, null);
      }
    );
  }
}
