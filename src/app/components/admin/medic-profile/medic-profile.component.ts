import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'app/shared/services/alert.service';
import { CommonService } from 'app/shared/services/glucome';
import { MedicService } from 'app/shared/services/glucome/medic.service';
import { environment } from 'environments/environment';
import { first } from 'rxjs/operators';

import { getCurrentUser, setCurrentUser } from '../../../helpers/storage';

@Component({
  selector: "app-medic-profile",
  templateUrl: "./medic-profile.component.html",
  styleUrls: ["./medic-profile.component.scss"]
})
export class MedicProfileComponent implements OnInit {
  file: any;
  user: any;
  urlBackend: any;
  apiUrl: string;
  uploadForm: FormGroup;
  editForm: FormGroup;
  submitted: boolean;

  constructor(
    private fb: FormBuilder,
    private commonService: CommonService,
    private formBuilder: FormBuilder,
    private medicService: MedicService,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    this.user = getCurrentUser();
    this.urlBackend = environment.BACKEND_URL;
    this.apiUrl = environment.API_URL;

    this.uploadForm = this.fb.group({
      profile: [""]
    });

    this.editForm = this.formBuilder.group({
      name: new FormControl(this.user.asociado.nombre, Validators.required),
      enrollment: new FormControl(this.user.asociado.matricula, Validators.required),
      description: new FormControl(this.user.asociado.descripcion)
    });
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get("profile").setValue(file);
    }
  }
  get formControls() {
    return this.editForm.controls;
  }

  onEditSubmit() {
    this.submitted = true;

    if (this.editForm.invalid) {
      return;
    }

    this.medicService
      .edit(this.user.asociado.id, this.formControls.name.value, this.formControls.enrollment.value, this.formControls.description.value)
      .pipe(first())
      .subscribe(
        data => {
          this.user.asociado = data.respuesta.asociado;
          setCurrentUser(this.user);
          this.alertService.showNotification("success", "Éxito", "Médico grabado correctamente", null);
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  onSubmit() {
    const formData = new FormData();
    formData.append("file", this.uploadForm.get("profile").value);

    this.commonService.uploadImage(formData).subscribe(res => {
      window.location.reload();
    });
  }

  deletePosition() {
    this.medicService.deletePosition().subscribe(
      res => {
        this.alertService.showNotification("success", "Éxito", "Ubicación borrada correctamente", null);
      },
      error => {
        this.alertService.showNotification("error", "Error", error, null);
      }
    );
  }

  getEnrollmentError() {
    return "La matrícula es obligatoria";
  }

  getNameError() {
    return "El nombre y apellido es obligatorio";
  }

  getDescriptionError() {
    return "La descripción es inválida";
  }
}
