import { Component, OnInit } from '@angular/core';

import { AlertService } from '../../../../shared/services/alert.service';
import { MedicService } from '../../../../shared/services/glucome/medic.service';

declare let tomtom: any;

@Component({
  selector: "app-position",
  templateUrl: "./position.component.html",
  styleUrls: ["./position.component.css"]
})
export class PositionComponent implements OnInit {
  CENTER_LATITUDE = -34.6076;
  CENTER_LONGITUDE = -58.4371;
  CENTER_ZOOM = 14;
  MARKER_ZOOM = 16;

  map: any;
  marker: any;
  showCurrentPosition = false;

  constructor(private medicService: MedicService, private alertService: AlertService) {}

  ngOnInit() {
    this.map = tomtom.L.map("map", {
      key: "duf5GGAtsoQFbJqSNHqQ70hTvbcmBaYN",
      container: "map",
      center: [this.CENTER_LATITUDE, this.CENTER_LONGITUDE],
      zoom: this.CENTER_ZOOM,
      language: "es-ES"
    });

    const self = this;
    this.map.on("click", function(ev) {
      self.setMarkerPosition(ev.latlng);
      self.showCurrentPosition = false;
    });

    this.marker = tomtom.L.marker([this.CENTER_LATITUDE, this.CENTER_LONGITUDE]).addTo(this.map);

    this.medicService.getPosition().subscribe(
      res => {
        const position = res["respuesta"].ubicacion.split(";");
        const latLng = tomtom.L.latLng(position[0], position[1]);
        this.setMarkerPosition(latLng);
      },
      error => {
        this.alertService.showNotification("error", "Error", error, null);
      }
    );
  }

  setCurrentPosition() {
    this.showCurrentPosition = !this.showCurrentPosition;
    if (this.showCurrentPosition) {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
          const currentPosition = tomtom.L.latLng(position.coords.latitude, position.coords.longitude);
          this.setMarkerPosition(currentPosition);
        });
      } else {
        this.alertService.showNotification("error", "Error", "La geolocalización no es soportada por este navegador", null);
        this.showCurrentPosition = false;
      }
    }
  }

  setMarkerPosition(latLng: any) {
    this.marker.setLatLng(latLng);
    this.map.setView(latLng, this.MARKER_ZOOM);
  }

  savePosition() {
    const latLng = this.marker.getLatLng();

    if (!latLng.lat || !latLng.lng) {
      this.alertService.showNotification("error", "Error", "Debe marcar su ubicación en el mapa", null);
      return;
    }

    const position = latLng.lat + ";" + latLng.lng;
    this.medicService.savePosition(position).subscribe(
      res => {
        this.alertService.showNotification("success", "Éxito", "Ubicación guardada correctamente", null);
      },
      error => {
        this.alertService.showNotification("error", "Error", error, null);
      }
    );
  }
}
