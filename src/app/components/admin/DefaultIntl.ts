import { OwlDateTimeIntl } from "ng-pick-datetime";

export class DefaultIntl extends OwlDateTimeIntl {
    /** A label for the cancel button */
    cancelBtnLabel= 'Cancelar';
  
    /** A label for the set button */
    setBtnLabel= 'Aceptar';
  };