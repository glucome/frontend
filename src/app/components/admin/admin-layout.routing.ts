import { Routes } from '@angular/router';
import { Permission } from 'app/enum/Permission';

import { getRouteWithPermissions } from '../../helpers/permissions';
import { PatientDetailComponent } from '../admin/patient/patient-detail/patient-detail.component';
import { PatientListComponent } from '../admin/patient/patient-list/patient-list.component';
import { PatientMeasurementComponent } from '../admin/patient/patient-measurement/patient-measurement.component';
import { PatientNewMeasurementComponent } from '../admin/patient/patient-new-measurement/patient-new-measurement.component';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MedicProfileComponent } from './medic-profile/medic-profile.component';
import { PositionComponent } from './medic-profile/position/position.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PatientExercisesComponent } from './patient/patient-exercises/patient-exercises.component';
import { PatientFoodComponent } from './patient/patient-food/patient-food.component';
import { PatientHistoryEditComponent } from './patient/patient-history-edit/patient-history-edit.component';
import { PatientHistoryComponent } from './patient/patient-history/patient-history.component';
import { PatientInsulinComponent } from './patient/patient-insulin/patient-insulin.component';
import { PatientMedicineComponent } from './patient/patient-medicine/patient-medicine.component';
import { PatientComponent } from './patient/patient/patient.component';
import { ReportFoodRankingComponent } from './report/food-ranking/report-food-ranking.component';
import { ReportByAgeRangeComponent } from './report/report-by-age-range/report-by-age-range.component';
import { ReportByDiabetesTypeComponent } from './report/report-by-diabetes-type/report-by-diabetes-type.component';
import { ReportByMeasurementTypeComponent } from './report/report-by-measurement-type/report-by-measurement-type.component';
import { ReportByMomentComponent } from './report/report-by-moment/report-by-moment.component';
import { ReportNumberMeasurementsComponent } from './report/report-number-measurements/report-number-measurements.component';
import { SubscriptionFailureComponent } from './subscription/subscription-failure/subscription-failure.component';
import { SubscriptionPendingComponent } from './subscription/subscription-pending/subscription-pending.component';
import { SubscriptionSuccessComponent } from './subscription/subscription-success/subscription-success.component';
import { SubscriptionComponent } from './subscription/subscription/subscription.component';

export const AdminLayoutRoutes: Routes = [
  { path: "", redirectTo: "dashboard", pathMatch: "full" },
  getRouteWithPermissions("dashboard", DashboardComponent, Permission.BASIC),
  getRouteWithPermissions("medic-profile", MedicProfileComponent, Permission.BASIC),
  getRouteWithPermissions("medic-profile/position", PositionComponent, Permission.BASIC),

  /*******************************
   * PATIENTS
   *******************************/
  getRouteWithPermissions("patients", PatientListComponent, Permission.BASIC),
  getRouteWithPermissions("patient/:id", PatientComponent, Permission.PREMIUM),
  getRouteWithPermissions("patient-detail", PatientDetailComponent, Permission.BASIC),
  getRouteWithPermissions("patient-exercises", PatientExercisesComponent, Permission.BASIC),
  getRouteWithPermissions("patient-food", PatientFoodComponent, Permission.BASIC),
  getRouteWithPermissions("patient-measurement", PatientMeasurementComponent, Permission.BASIC),
  getRouteWithPermissions("patient-new-measurement", PatientNewMeasurementComponent, Permission.BASIC),
  getRouteWithPermissions("patient-history", PatientHistoryComponent, Permission.BASIC),
  //Se repite el patient history porque si no el nuevo no funciona
  getRouteWithPermissions("patient-history-edit", PatientHistoryEditComponent, Permission.BASIC),
  getRouteWithPermissions("patient-history-edit/:id", PatientHistoryEditComponent, Permission.BASIC),
  getRouteWithPermissions("patient-medicine", PatientMedicineComponent, Permission.BASIC),
  getRouteWithPermissions("patient-insulin", PatientInsulinComponent, Permission.BASIC),

  /*******************************
   * REPORTS
   *******************************/
  getRouteWithPermissions("report/number-measurements", ReportNumberMeasurementsComponent, Permission.PREMIUM),
  getRouteWithPermissions("report/by-moment", ReportByMomentComponent, Permission.PREMIUM),
  getRouteWithPermissions("report/by-age-range", ReportByAgeRangeComponent, Permission.PREMIUM),
  getRouteWithPermissions("report/by-diabetes-type", ReportByDiabetesTypeComponent, Permission.PREMIUM),
  getRouteWithPermissions("report/by-measurement-type", ReportByMeasurementTypeComponent, Permission.PREMIUM),
  getRouteWithPermissions("report/food-ranking", ReportFoodRankingComponent, Permission.PREMIUM),

  /*******************************
   * SUBSCRIPTION
   *******************************/
  getRouteWithPermissions("subscription", SubscriptionComponent, Permission.BASIC),
  getRouteWithPermissions("subscription-success", SubscriptionSuccessComponent, Permission.BASIC),
  getRouteWithPermissions("subscription-pending", SubscriptionPendingComponent, Permission.BASIC),
  getRouteWithPermissions("subscription-failure", SubscriptionFailureComponent, Permission.BASIC),

  getRouteWithPermissions("access-denied", AccessDeniedComponent, Permission.BASIC),
  getRouteWithPermissions("not-found", NotFoundComponent, Permission.BASIC),
  { path: "**", redirectTo: "not-found" }
];
