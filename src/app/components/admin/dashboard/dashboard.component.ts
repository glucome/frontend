import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DiabetesType } from 'app/enum/DiabetesTypeEnum';
import { MeasurementType } from 'app/enum/MeasurementTypeEnum';
import { getCurrentUser, setCurrentPatient } from 'app/helpers/storage';
import { AlertService } from 'app/shared/services/alert.service';
import { PatientService } from 'app/shared/services/glucome';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { environment } from 'environments/environment';
import { Label } from 'ng2-charts';
import { first } from 'rxjs/operators';
import { $enum } from 'ts-enum-util';

import { AgeRange } from '../../../enum/AgeRangeEnum';
import { DayMoment } from '../../../enum/DayMomentEnum';
import { getDefaultBarChartOptions } from '../../../helpers/chart';
import { MedicReportService } from '../../../shared/services/glucome/medic-report.service';

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  loadingReportByMoment = true;
  loadingReportByAgeRange = true;
  loadingReportByDiabetesType = true;
  loadingReportByMeasurementType = true;

  public barChartOptions: ChartOptions = getDefaultBarChartOptions();

  public barChartLabels: Label[] = $enum(DayMoment).getKeys();
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartReportByMomentData: ChartDataSets[] = [];
  public barChartReportByAgeRangeData: ChartDataSets[] = [];
  public barChartReportByDiabetesTypeData: ChartDataSets[] = [];
  public barChartReportByMeasurementTypeData: ChartDataSets[] = [];

  public urlBackend: any;
  public apiUrl: any;
  public headElements = ["", "Nombre", "Valor", "Momento", "Fecha Hora", "Acción"];
  public measurementLog: any;
  loadingMeasurementLog: boolean;

  constructor(
    private medicReportService: MedicReportService,
    private patientService: PatientService,
    private alertService: AlertService,
    private router: Router
  ) {}

  ngOnInit() {
    this.fillChartReportByMoment();
    this.fillChartReportByAgeRange();
    this.fillChartReportByDiabetesType();
    this.fillChartReportByMeasurementType();
    this.getMeasurementLog();
  }
  getMeasurementLog() {
    this.loadingMeasurementLog = true;
    this.urlBackend = environment.BACKEND_URL;
    this.apiUrl = environment.API_URL;
    const user = getCurrentUser();
    let id = user.asociado.id;
    this.patientService
      .getMeasurementLog(id)
      .pipe(first())
      .subscribe(
        data => {
          this.measurementLog = data["respuesta"].logMedicion;
          this.loadingMeasurementLog = false;
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  goToPatient(patientId) {
    this.patientService
      .getPatient(patientId)
      .pipe(first())
      .subscribe(
        data => {
          setCurrentPatient(data["respuesta"].paciente);
          this.router.navigate(["patient-detail"]);
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  getDayMoment(moment) {
    return DayMoment[moment];
  }

  clearChartReportByMoment() {
    this.barChartReportByMomentData = [];
  }

  clearChartReportByAgeRange() {
    this.barChartReportByAgeRangeData = [];
  }

  clearChartReportByDiabetesType() {
    this.barChartReportByDiabetesTypeData = [];
  }

  clearChartReportByMeasurementType() {
    this.barChartReportByMeasurementTypeData = [];
  }

  fillChartReportByMoment() {
    this.loadingReportByMoment = true;
    this.clearChartReportByMoment();

    let data = new Array(this.barChartLabels.length);

    this.medicReportService.getReportByMoment().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];

      reportResponse.map(report => {
        data.splice(report.momento, 0, report.promedio);
      });
    });

    this.barChartReportByMomentData.push({ data: data, label: "" });

    this.loadingReportByMoment = false;
  }

  fillChartReportByAgeRange() {
    this.loadingReportByAgeRange = true;
    this.clearChartReportByAgeRange();

    let ageRange0To9 = new Array(this.barChartLabels.length);
    let ageRange10To24 = new Array(this.barChartLabels.length);
    let ageRange25To34 = new Array(this.barChartLabels.length);
    let ageRange35To44 = new Array(this.barChartLabels.length);
    let ageRange45To54 = new Array(this.barChartLabels.length);
    let ageRange55To64 = new Array(this.barChartLabels.length);
    let ageRange65To74 = new Array(this.barChartLabels.length);
    let ageRange75To84 = new Array(this.barChartLabels.length);
    let ageRange85To94 = new Array(this.barChartLabels.length);
    let ageRange95To104 = new Array(this.barChartLabels.length);
    let ageRange105To114 = new Array(this.barChartLabels.length);

    this.medicReportService.getReportByAgeRange().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];
      reportResponse.map(report => {
        switch (report.rangoEdad) {
          case AgeRange["0-9"]:
            ageRange0To9.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["10-24"]:
            ageRange10To24.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["25-34"]:
            ageRange25To34.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["35-44"]:
            ageRange35To44.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["45-54"]:
            ageRange45To54.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["55-64"]:
            ageRange55To64.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["65-74"]:
            ageRange65To74.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["75-84"]:
            ageRange75To84.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["85-94"]:
            ageRange85To94.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["95-104"]:
            ageRange95To104.splice(report.momento, 0, report.promedio);
            break;
          case AgeRange["105-114"]:
            ageRange105To114.splice(report.momento, 0, report.promedio);
            break;
        }
      });
    });

    this.barChartReportByAgeRangeData.push({ data: ageRange0To9, label: $enum(AgeRange).getKeyOrThrow(AgeRange["0-9"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange10To24, label: $enum(AgeRange).getKeyOrThrow(AgeRange["10-24"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange25To34, label: $enum(AgeRange).getKeyOrThrow(AgeRange["25-34"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange35To44, label: $enum(AgeRange).getKeyOrThrow(AgeRange["35-44"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange45To54, label: $enum(AgeRange).getKeyOrThrow(AgeRange["45-54"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange55To64, label: $enum(AgeRange).getKeyOrThrow(AgeRange["55-64"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange65To74, label: $enum(AgeRange).getKeyOrThrow(AgeRange["65-74"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange75To84, label: $enum(AgeRange).getKeyOrThrow(AgeRange["75-84"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange85To94, label: $enum(AgeRange).getKeyOrThrow(AgeRange["85-94"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange95To104, label: $enum(AgeRange).getKeyOrThrow(AgeRange["95-104"]) });
    this.barChartReportByAgeRangeData.push({ data: ageRange105To114, label: $enum(AgeRange).getKeyOrThrow(AgeRange["105-114"]) });

    this.loadingReportByAgeRange = false;
  }

  fillChartReportByDiabetesType() {
    this.loadingReportByDiabetesType = true;
    this.clearChartReportByDiabetesType();

    let diabetesType1Data = new Array(this.barChartLabels.length);
    let diabetesType2Data = new Array(this.barChartLabels.length);
    let diabetesTypeGestationalData = new Array(this.barChartLabels.length);
    let diabetesTypeOtherData = new Array(this.barChartLabels.length);

    this.medicReportService.getReportByDiabetesType().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];
      reportResponse.map(report => {
        switch (report.tipoDiabetes) {
          case DiabetesType["Tipo 1"]:
            diabetesType1Data.splice(report.momento, 0, report.promedio);
            break;
          case DiabetesType["Tipo 2"]:
            diabetesType2Data.splice(report.momento, 0, report.promedio);
            break;
          case DiabetesType.Gestacional:
            diabetesTypeGestationalData.splice(report.momento, 0, report.promedio);
            break;
          case DiabetesType["N/A"]:
            diabetesTypeOtherData.splice(report.momento, 0, report.promedio);
            break;
        }
      });

      this.barChartReportByDiabetesTypeData.push({ data: diabetesType1Data, label: $enum(DiabetesType).getKeyOrThrow(DiabetesType["Tipo 1"]) });
      this.barChartReportByDiabetesTypeData.push({ data: diabetesType2Data, label: $enum(DiabetesType).getKeyOrThrow(DiabetesType["Tipo 2"]) });
      this.barChartReportByDiabetesTypeData.push({
        data: diabetesTypeGestationalData,
        label: $enum(DiabetesType).getKeyOrThrow(DiabetesType.Gestacional)
      });
      this.barChartReportByDiabetesTypeData.push({ data: diabetesTypeOtherData, label: $enum(DiabetesType).getKeyOrThrow(DiabetesType["N/A"]) });

      this.loadingReportByDiabetesType = false;
    });
  }

  fillChartReportByMeasurementType() {
    this.loadingReportByMeasurementType = true;
    this.clearChartReportByMeasurementType();

    let invasiveData = new Array(this.barChartLabels.length);
    let nonInvasiveData = new Array(this.barChartLabels.length);
    let otherData = new Array(this.barChartLabels.length);

    this.medicReportService.getReportByMeasurementType().subscribe(resp => {
      const reportResponse = resp["respuesta"]["reporte"];
      reportResponse.map(report => {
        switch (report.tipoMedicion) {
          case MeasurementType.Invasiva:
            invasiveData.splice(report.momento, 0, report.promedio);
            break;
          case MeasurementType["No invasiva"]:
            nonInvasiveData.splice(report.momento, 0, report.promedio);
            break;
          case MeasurementType["No aplica"]:
            otherData.splice(report.momento, 0, report.promedio);
            break;
        }
      });

      this.barChartReportByMeasurementTypeData.push({ data: invasiveData, label: $enum(MeasurementType).getKeyOrThrow(MeasurementType.Invasiva) });
      this.barChartReportByMeasurementTypeData.push({
        data: nonInvasiveData,
        label: $enum(MeasurementType).getKeyOrThrow(MeasurementType["No invasiva"])
      });
      this.barChartReportByMeasurementTypeData.push({ data: otherData, label: $enum(MeasurementType).getKeyOrThrow(MeasurementType["No aplica"]) });

      this.loadingReportByMeasurementType = false;
    });
  }
}
