import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'app/shared/services/alert.service';
import { AuthenticationService } from 'app/shared/services/authentication.service';
import { AuthService } from 'app/shared/services/glucome';
import { first } from 'rxjs/operators';

import { CustomValidators, setBodyBackground } from '../../helpers';

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  submitted = false;
  hidePassword = true;
  hideConfirmPassword = true;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private authService: AuthService,
    private customValidators: CustomValidators,
    private alertService: AlertService
  ) {
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(["/"]);
    }
  }

  ngOnInit() {
    setBodyBackground();

    this.signupForm = this.formBuilder.group(
      {
        email: new FormControl("", Validators.compose([Validators.required, Validators.email]), this.customValidators.emailTaken("email")),
        username: new FormControl("", Validators.required, this.customValidators.usernameTaken("username")),
        password: new FormControl("", Validators.required),
        confirmPassword: new FormControl("", Validators.required),
        name: new FormControl("", Validators.required),
        enrollment: new FormControl("", Validators.required)
      },
      {
        validator: this.customValidators.mustMatch("password", "confirmPassword")
      }
    );
  }

  get formControls() {
    return this.signupForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.signupForm.invalid) {
      return;
    }

    this.authService
      .register(
        this.formControls.email.value,
        this.formControls.username.value,
        this.formControls.password.value,
        this.formControls.name.value,
        this.formControls.enrollment.value
      )
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(["/"]);
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  getEmailError() {
    if (this.formControls["email"].hasError("required")) {
      return "La dirección de correo electrónico es obligatoria.";
    } else if (this.formControls["email"].hasError("email")) {
      return "La dirección de correo electrónico es invalida.";
    } else if (this.formControls["email"].hasError("emailTaken")) {
      return "La dirección de correo electrónico ya se encuentra en uso.";
    }
  }

  getUsernameError() {
    if (this.formControls["username"].hasError("required")) {
      return "El nombre de usuario es obligatorio.";
    } else if (this.formControls["username"].hasError("usernameTaken")) {
      return "El nombre de usuario ya se encuentra en uso.";
    }
  }

  public getPasswordError() {
    if (this.formControls["password"].hasError("required")) {
      return "La contraseña es obligatoria.";
    }
  }

  public getConfirmPasswordError() {
    if (this.formControls["confirmPassword"].hasError("required")) {
      return "La confirmación de la contraseña es obligatoria.";
    } else if (this.formControls["confirmPassword"].hasError("mustMatch")) {
      return "Las contraseñas no coinciden. Inténtalo de nuevo.";
    }
  }

  public getNameError() {
    if (this.formControls["name"].hasError("required")) {
      return "El nombre es obligatorio.";
    }
  }

  public getEnrollmentError() {
    if (this.formControls["enrollment"].hasError("required")) {
      return "La matrícula es obligatoria.";
    }
  }
}
