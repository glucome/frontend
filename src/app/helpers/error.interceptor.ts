import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpError } from 'app/enum/HttpError';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AuthenticationService } from '../shared/services/authentication.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError(err => {
        if (err.status === HttpError.UNAUTHORIZED) {
          this.authenticationService.logout();
          location.reload(true);
        }

        if (err.status >= HttpError.INTERNAL_SERVER_ERROR_BEGIN && err.status <= HttpError.INTERNAL_SERVER_ERROR_END) {
          err.error = { mensaje: "Ha ocurrido un error en la comunicación con el servidor" };
        }

        const error = err.error.mensaje || err.error.message || err.statusText;
        return throwError(error);
      })
    );
  }
}
