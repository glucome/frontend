export function convertDate(inputFormat) {
  if (inputFormat == "") {
    return inputFormat;
  }
  function pad(s) {
    return s < 10 ? "0" + s : s;
  }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("/");
}

export function convertDateTime(inputFormat) {
  if (inputFormat == "") {
    return inputFormat;
  }
  function pad(s) {
    return s < 10 ? "0" + s : s;
  }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join("/") + " " + pad(d.getHours()) + ":" + pad(d.getMinutes());
}

export function getDateFromStringValueDatePicker(stringDate) {
  if (stringDate == "") {
    return stringDate;
  }
  let dateSplit = stringDate.split("/");
  return new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0], 0, 0, 0, 0);
}

export function getDateTimeFromStringValueDatePicker(stringDate) {
  if (stringDate == "") {
    return stringDate;
  }
  let dateSplit = stringDate.split(" ");
  let hoursAndMinutes = dateSplit[1];
  hoursAndMinutes = hoursAndMinutes.split(":");
  let hours = hoursAndMinutes[0];
  let minutes = hoursAndMinutes[1];
  dateSplit = dateSplit[0].split("/");
  return new Date(dateSplit[2], dateSplit[1] - 1, dateSplit[0], hours, minutes, 0, 0);
}
