import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { AuthService } from '../shared/services/glucome';

@Injectable({ providedIn: "root" })
export class CustomValidators {
  constructor(private authService: AuthService) {}

  public mustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      // return if another validator has already found an error on the matchingControl
      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  public emailTaken(controlName: string) {
    return (control: FormControl) => {
      if (control.errors && !control.errors.emailTaken) {
        return;
      }

      return this.authService.emailTaken(control.value);
    };
  }

  public usernameTaken(controlName: string) {
    return (control: FormControl) => {
      if (control.errors && !control.errors.userNameTaken) {
        return;
      }

      return this.authService.usernameTaken(control.value);
    };
  }

  public lessThan(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.lessThan) {
        return;
      }

      if (control.value > matchingControl.value) {
        matchingControl.setErrors({ lessThan: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  public notNumber(controlName: string) {
    return (control: FormControl) => {
      if (control.errors && !control.errors.notNumber) {
        return;
      }

      return !isNaN(control.value) ? null : { notNumber: true };
    };
  }
}
