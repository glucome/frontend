export const getCurrentUser: Function = () => JSON.parse(localStorage.getItem("currentUser"));
export const setCurrentUser: Function = user => localStorage.setItem("currentUser", JSON.stringify(user));

export const getCurrentPatient: Function = () => JSON.parse(localStorage.getItem("currentPatient"));
export const setCurrentPatient: Function = patient => localStorage.setItem("currentPatient", JSON.stringify(patient));

export const getMPInitPoint: Function = () => JSON.parse(localStorage.getItem("MPInitPoint"));
export const setMPInitPoint: Function = initPoint => localStorage.setItem("MPInitPoint", JSON.stringify(initPoint));
