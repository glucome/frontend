import { Type } from '@angular/core';
import { Route } from '@angular/router';
import { NgxPermissionsGuard } from 'ngx-permissions';

import { Role } from '../enum/Role';
import { getCurrentUser } from './storage';

export const getRouteWithPermissions: Function = (path: string, component: Type<any>, permission: Array<Role>): Route => {
  return {
    path: path,
    component: component,
    canActivate: [NgxPermissionsGuard],
    data: {
      permissions: {
        only: permission,
        redirectTo: "/access-denied"
      }
    }
  };
};

export function isPremium() {
  return getCurrentUser().roles[0] === Role.ROLE_MEDIC_PREMIUM;
}
