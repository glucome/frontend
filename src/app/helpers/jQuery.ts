import * as $ from 'jquery';

export const setBodyBackground: Function = () => $("#page-top").addClass("bg-gradient-primary");

export const setChatClasses: Function = () => {
  $("#ng-chat-search_friend").addClass("form-control");
};
