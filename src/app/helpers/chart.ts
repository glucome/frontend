import { ChartOptions } from 'chart.js';
import Decimal from 'decimal.js';

export const getDefaultBarChartOptions: Function = (xAxeLabel: string, yAxeLabel: string, tooltipLabelText: string): ChartOptions => {
  let barchartOptions: ChartOptions = {
    responsive: true,
    tooltips: {
      callbacks: {
        label: function(tooltipItem) {
          return new Decimal(tooltipItem.yLabel).toDecimalPlaces(2, Decimal.ROUND_UP) + (tooltipLabelText !== undefined ? tooltipLabelText : "");
        }
      }
    },
    scales: {
      xAxes: [
        {
          scaleLabel: {
            display: true,
            fontSize: 20
          }
        }
      ],
      yAxes: [
        {
          scaleLabel: {
            display: true,
            fontSize: 20
          },
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    plugins: {
      datalabels: {
        anchor: "end",
        align: "end",
        display: false
      }
    }
  };

  setLabels(barchartOptions, xAxeLabel, yAxeLabel);
  return barchartOptions;
};

export const setLabels: Function = (barChartOptions: ChartOptions, xAxeLabel: string, yAxeLabel: string) => {
  if (xAxeLabel) {
    barChartOptions.scales.xAxes[0].scaleLabel.labelString = xAxeLabel;
  }

  if (yAxeLabel) {
    barChartOptions.scales.yAxes[0].scaleLabel.labelString = yAxeLabel;
  }
};
