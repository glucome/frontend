export enum MeasurementType {
  "Invasiva" = 0,
  "No invasiva" = 1,
  "No aplica" = 2
}
