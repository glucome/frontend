export class HttpError {
  static BAD_REQUEST = 400;
  static UNAUTHORIZED = 401;
  static FORBIDDEN = 403;
  static NOT_FOUND = 404;
  static TIME_OUT = 408;
  static CONFLICT = 409;
  static INTERNAL_SERVER_ERROR_BEGIN = 500;
  static INTERNAL_SERVER_ERROR_END = 599;
}
