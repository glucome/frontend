export enum InsulinType {
    "Regular / normal" = 0,
    "Lyspro/ Aspart/ Glulisina" = 1,
    "NPH" = 2,
    "Detemir" = 3,
    "Glargina" = 4,
    "Otro" = 5,
  }

