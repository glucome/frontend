export enum DiabetesType {
  "Tipo 1" = 0,
  "Tipo 2" = 1,
  "Gestacional" = 2,
  "N/A" = 3
}
