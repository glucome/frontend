export enum DayMoment {
  "Antes del desayuno" = 0,
  "Después del desayuno" = 1,
  "Antes del almuerzo" = 2,
  "Después del almuerzo" = 3,
  "Antes de la merienda" = 4,
  "Después de la merienda" = 5,
  "Antes de la cena" = 6,
  "Después de la cena" = 7,
  "No aplica" = 8
}
