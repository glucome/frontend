export enum Sex {
  "Masculino" = 0,
  "Femenino" = 1,
  "Otro" = 2
}
