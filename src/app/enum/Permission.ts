import { Role } from './Role';

export const Permission = {
  BASIC: [Role.ROLE_MEDIC_BASIC, Role.ROLE_MEDIC_PREMIUM],
  PREMIUM: [Role.ROLE_MEDIC_PREMIUM]
};
