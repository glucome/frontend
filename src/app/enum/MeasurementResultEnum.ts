export enum MeasurementResult {
  "bad-value" = 0,
  "medium-value" = 1,
  "good-value" = 2
}
