import { IChatParticipant } from 'ng-chat';

export interface ChatParticipant extends IChatParticipant {
  chatId: number;
  totalUnreadMessages: number;
}
