import { Message } from 'ng-chat';

export interface ChatMessage extends Message {
  dateSent?: any;
  dateSeen?: any;
}
