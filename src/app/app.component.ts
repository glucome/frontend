import { Component, OnInit } from '@angular/core';
import { NgxPermissionsService } from 'ngx-permissions';

import { getCurrentUser } from './helpers/storage';

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "app";

  constructor(private permissionsService: NgxPermissionsService) {}

  ngOnInit(): void {
    if (getCurrentUser()) {
      this.permissionsService.loadPermissions(getCurrentUser().roles);
    }
  }
}
