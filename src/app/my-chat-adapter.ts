import 'rxjs/add/operator/map';

import { RxStompService } from '@stomp/ng2-stompjs';
import { Message as STOMPMessage } from '@stomp/stompjs';
import * as moment from 'moment';
import { ChatAdapter, ChatParticipantStatus, ChatParticipantType, ParticipantResponse } from 'ng-chat';
import { Observable } from 'rxjs';

import { environment } from '../environments/environment';
import { getCurrentUser } from './helpers/storage';
import { ChatMessage } from './models/chat-message.model';
import { ChatParticipant } from './models/chat-participant.model';
import { ChatService } from './shared/services/glucome';

export class MyChatAdapter extends ChatAdapter {
  private DATE_TIME_FORMAT = "YYYY-MM-DDTHH:mm:ss";

  private userId;
  private participants: ChatParticipant[] = [];
  public subscription;

  constructor(private rxStompService: RxStompService, private chatService: ChatService) {
    super();
    this.userId = getCurrentUser().id;
    this.initializeConnection();
  }

  initializeConnection() {
    this.chatService.changeState(this.userId, true).subscribe();

    this.subscription = this.rxStompService.watch(`/socket-publisher/${this.userId}`).subscribe((message: STOMPMessage) => {
      const messageJSON = JSON.parse(message.body);
      const participant = this.participants.find(participant => participant.id === messageJSON.fromId);
      this.onMessageReceived(participant, messageJSON);
    });
  }

  getPatients() {
    this.participants = [];

    return this.chatService.getPatientsChat(this.userId).map(res => {
      const patients = res["respuesta"].usuariosChats;

      for (const patient of patients) {
        this.participants.push({
          participantType: ChatParticipantType.User,
          id: patient.idUsuario,
          displayName: patient.nombre,
          avatar: `${environment.API_URL}/imagen/usuario/${patient.idUsuario}/`,
          status: patient.estado === "ONLINE" ? ChatParticipantStatus.Online : ChatParticipantStatus.Offline,
          chatId: patient.idChat,
          totalUnreadMessages: patient.cantidadMensajesNoLeidos
        });
      }

      return this.participants;
    });
  }

  listFriends(): Observable<ParticipantResponse[]> {
    return this.getPatients().map(participants =>
      participants.map(user => {
        let participantResponse = new ParticipantResponse();

        participantResponse.participant = user;
        participantResponse.metadata = {
          totalUnreadMessages: this.participants.find(participant => participant.id === user.id).totalUnreadMessages
        };

        return participantResponse;
      })
    );
  }

  getMessageHistory(destinataryId: any): Observable<ChatMessage[]> {
    const chatId = this.participants.find(participant => participant.id === destinataryId).chatId;
    return this.chatService.getMessageHistory(chatId);
  }

  sendMessage(message: ChatMessage): void {
    message["chatId"] = this.participants.find(participant => participant.id === message.fromId || participant.id === message.toId).chatId;
    message.dateSent = moment().format(this.DATE_TIME_FORMAT);
    this.rxStompService.publish({ destination: "/socket-subscriber/send/message", body: JSON.stringify(message) });
  }

  markMessagesAsRead(messages: ChatMessage[]) {
    if (messages.length == 0) return;

    const dateSeen = moment().format(this.DATE_TIME_FORMAT);

    messages.map(message => {
      message.dateSeen = dateSeen;
      return message;
    });

    const chatId = this.participants.find(participant => participant.id === messages[0].fromId || participant.id === messages[0].toId).chatId;
    this.chatService.markMessagesAsRead(chatId, this.userId, dateSeen).subscribe();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
