import 'hammerjs';
import 'mousetrap';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { GalleryModule } from '@ks89/angular-modal-gallery';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { NgxPermissionsModule } from 'ngx-permissions';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { ErrorInterceptor, JwtInterceptor } from './helpers';
import { myRxStompConfig } from './my-rx-stomp.config';

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    ComponentsModule,
    HttpModule,
    HttpClientModule,
    RouterModule,
    NgxPermissionsModule.forRoot(),
    GalleryModule.forRoot()
  ],
  declarations: [AppComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: InjectableRxStompConfig, useValue: myRxStompConfig },
    { provide: RxStompService, useFactory: rxStompServiceFactory, deps: [InjectableRxStompConfig] }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
