import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: "root"
})
export class RequestService {
  constructor(private http: HttpClient) {}

  public getRequest(query: string, params: object) {
    let httpParams = new HttpParams();

    for (let [key, value] of (<any>Object).entries(params)) {
      httpParams = httpParams.append(key, value);
    }

    return this.http.get(`${environment.API_URL}/${query}/`, { params: httpParams });
  }

  public postRequest(query: string, body: object) {
    return this.http.post<any>(`${environment.API_URL}/${query}/`, body);
  }

  public postImage(url: string, formData: FormData) {
    return this.http.post<any>(`${environment.API_URL}/${url}/`, formData);
  }

  public putRequest(query: string, body: object) {
    return this.http.put<any>(`${environment.API_URL}/${query}/`, body);
  }

  public deleteRequest(query: string, params: object) {
    let httpParams = new HttpParams();

    for (let [key, value] of (<any>Object).entries(params)) {
      httpParams = httpParams.append(key, value);
    }

    return this.http.delete(`${environment.API_URL}/${query}/`, { params: httpParams });
  }

  public downloadRequest(query: string, fileName: string, body) {
    let httpParams = new HttpParams();

    for (let [key, value] of (<any>Object).entries(body)) {
      httpParams = httpParams.append(key, value);
    }

    return this.http
      .get(`${environment.API_URL}/${query}/`, { responseType: "arraybuffer", params: httpParams })
      .subscribe(response => this.downLoadFile(response, fileName));
  }

  downLoadFile(data: any, fileName: string) {
    let dataType = data.type;
    let binaryData = [];
    binaryData.push(data);
    let downloadLink = document.createElement("a");
    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: dataType }));
    downloadLink.setAttribute("download", fileName);
    document.body.appendChild(downloadLink);
    downloadLink.click();
  }
}
