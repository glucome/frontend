import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'app/models/user.model';
import { NgxPermissionsService } from 'ngx-permissions';
import { BehaviorSubject, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { getCurrentUser, setCurrentUser } from '../../helpers/storage';
import { AlertService } from './alert.service';
import { AuthService, ChatService } from './glucome';

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(
    private router: Router,
    private authService: AuthService,
    private chatService: ChatService,
    private alertService: AlertService,
    private permissionsService: NgxPermissionsService
  ) {
    this.currentUserSubject = new BehaviorSubject<User>(getCurrentUser());
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string, returnUrl: string) {
    this.authService
      .login(username, password)
      .pipe(first())
      .subscribe(
        data => {
          let user = data["respuesta"];
          if (user && user.token) {
            setCurrentUser(user);
            this.currentUserSubject.next(user);
            this.permissionsService.loadPermissions(getCurrentUser().roles);
          }

          this.router.navigate([returnUrl]);
        },
        error => {
          this.alertService.showNotification("error", "Error", error, null);
        }
      );
  }

  async logout() {
    const user = getCurrentUser();
    this.chatService.changeState(user.id, false).subscribe();
    localStorage.removeItem("currentUser");
    this.currentUserSubject.next(null);
    this.router.navigate(["/login"]);
  }
}
