import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class RequestPatientService {
  constructor(private requestService: RequestService) {}

  getLinkingRequest() {
    let user = getCurrentUser();
    return this.requestService.getRequest(`medico/${user.asociado.id}/solicitudes`, {});
  }

  acceptRequest(requestId: number) {
    return this.requestService.putRequest(`solicitud/${requestId}`, {});
  }

  rejectRequest(requestId: number) {
    return this.requestService.deleteRequest(`solicitud/${requestId}`, {});
  }
}
