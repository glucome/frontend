import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class SubscriptionService {
  constructor(private requestService: RequestService) {}

  activateSubscription(planId: number) {
    let user = getCurrentUser();
    return this.requestService.postRequest(`suscripcion/medico/${user.asociado.id}`, { id: planId });
  }
}
