import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class PaymentService {
  constructor(private requestService: RequestService) {}

  getPaymentsUnnotified() {
    const userId = getCurrentUser().id;
    return this.requestService.getRequest(`pago/${userId}/sin_notificar`, {});
  }

  markPaymentsAsNotified(paymentsIds: number[]) {
    return this.requestService.postRequest(`pago/marcar_notificados`, { idsPagos: paymentsIds });
  }
}
