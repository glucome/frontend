import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class PatientService {
  constructor(private requestService: RequestService) {}

  getPatients() {
    let user = getCurrentUser();
    return this.requestService.getRequest(`medico/${user.asociado.id}/pacientes`, {});
  }

  getLastMeasurement(patientId: number) {
    return this.requestService.getRequest(`paciente/${patientId}/ultimaMedicion`, {});
  }

  getLatestMeasurements(patientId: number) {
    return this.requestService.getRequest(`paciente/${patientId}/ultimas_mediciones`, {});
  }

  getMeasurements(idPatient: number, body: any) {
    return this.requestService.postRequest(`paciente/${idPatient}/medicion/listado`, body);
  }

  downloadMeasurements(patientId: number, fileName: string, body: any) {
    return this.requestService.downloadRequest(`paciente/${patientId}/mediciones/exportar`, fileName, body);
  }

  downloadHistory(patientId: any, medicId: any, fileName: string, filters: any) {
    return this.requestService.downloadRequest(`descargar_historia_clinica/${medicId}/${patientId}/`, fileName, filters);
  }

  getPatientHistory(patientId: any, body: { medico: any; fechaDesde: any; fechaHasta: any; descripcion: any; numeroPagina: number }) {
    return this.requestService.postRequest(`paciente/${patientId}/historia/listado`, body);
  }

  deleteHistory(patientId: number) {
    return this.requestService.deleteRequest(`paciente/historia/${patientId}`, {});
  }

  saveItemHistory(patientId: number, body: any, isEdit: boolean) {
    if (isEdit) {
      return this.requestService.putRequest(`paciente/${patientId}/historia/`, body);
    } else {
      return this.requestService.postRequest(`paciente/${patientId}/historia/`, body);
    }
  }

  getItemHistory(itemId: number) {
    return this.requestService.getRequest(`paciente/historia/${itemId}`, {});
  }

  getPatientMedicine(patientId: any) {
    return this.requestService.getRequest(`paciente/${patientId}/medicamentos`, {});
  }

  getMedicines(patientId: any, terms: any) {
    const params = {
      paciente: patientId,
      nombre: terms
    };
    return this.requestService.getRequest(`medicamento`, params);
  }

  savePatientMedicine(body: any) {
    return this.requestService.postRequest(`medicamento`, body);
  }

  getInjectedInsulin(
    idPatient: any,
    body: { fechaHoraDesde: any; fechaHoraHasta: any; unidadesDesde: any; unidadesHasta: any; tipo: any; numeroPagina: number }
  ) {
    return this.requestService.postRequest(`paciente/${idPatient}/insulina/listado`, body);
  }

  registerExternalPatient(idMedic: any, body) {
    return this.requestService.postRequest(`medico/${idMedic}/paciente`, body);
  }

  getExercises(
    idPatient: any,
    body: { fechaHoraDesde: any; fechaHoraHasta: any; minutosDesde: any; minutosHasta: any; tipo: any; numeroPagina: number }
  ) {
    return this.requestService.postRequest(`paciente/${idPatient}/ejercicio/listado`, body);
  }

  getPatientFoods(
    idPatient: any,
    body: { fechaHoraDesde: any; fechaHoraHasta: any; porcionesDesde: any; porcionesHasta: any; momento: any; numeroPagina: number }
  ) {
    return this.requestService.postRequest(`paciente/${idPatient}/alimento_ingerido/listado`, body);
  }

  getGlycosylatedHemoglobin(patientId: any) {
    return this.requestService.getRequest(`paciente/${patientId}/hemoglobina_glicosilada`, {});
  }

  getGlycosylatedHemoglobinYearly(patientId: any) {
    return this.requestService.getRequest(`paciente/${patientId}/hemoglobina_glicosilada_anual`, {});
  }

  newMeasurement(id: any, datetime: any, value: any, moment: any, type: any) {
    return this.requestService.postRequest(`paciente/${id}/medicion`, { fechaHora: datetime, valor: value, momento: moment, tipo: type });
  }

  newInsulineFactor(id: any, azucar: any, hidratos: any) {
    return this.requestService.postRequest(`paciente/${id}/factor_correccion_insulina`, {
      factorCorreccionAzucar: azucar,
      factorCorreccionCHO: hidratos
    });
  }

  getInsulinFactor(id: any) {
    return this.requestService.getRequest(`paciente/${id}/factor_correccion_insulina`, {});
  }

  getMeasurementLog(id) {
    return this.requestService.getRequest(`medico/${id}/pacientes/log_mediciones`, {});
  }

  getPatient(patientId: any) {
    return this.requestService.getRequest(`paciente/${patientId}`, {});
  }
}
