import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class MercadoPagoService {
  constructor(private requestService: RequestService) {}

  getInitPoint() {
    const userId = getCurrentUser().id;
    return this.requestService.getRequest(`mercadopago/init_point/medico/${userId}`, {});
  }
}
