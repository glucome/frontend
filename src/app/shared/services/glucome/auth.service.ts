import { Injectable } from '@angular/core';

import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(private requestService: RequestService) {}

  login(username: string, password: string) {
    return this.requestService.postRequest("auth/signin/medico", { usuario: username, password, requestWithToken: false }).map(res => {
      return res;
    });
  }

  register(email: string, username: string, password: string, name: string, enrollment: string) {
    return this.requestService
      .postRequest("medico", {
        mail: email,
        nombreUsuario: username,
        password: password,
        nombre: name,
        matricula: enrollment
      })
      .map(res => {
        return res;
      });
  }

  emailTaken(email: string) {
    return this.requestService.getRequest("existe_mail", { mail: email }).map(res => {
      return res["respuesta"].valor === false ? null : { emailTaken: true };
    });
  }

  usernameTaken(username: string) {
    return this.requestService.getRequest("existe_nombre_usuario", { nombreUsuario: username }).map(res => {
      return res["respuesta"].valor === false ? null : { usernameTaken: true };
    });
  }
}
