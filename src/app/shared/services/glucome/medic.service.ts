import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class MedicService {
  constructor(private requestService: RequestService) {}

  getRoles() {
    const userId = getCurrentUser().id;
    return this.requestService.getRequest(`usuario/${userId}/roles`, {});
  }

  edit(id: any, name: string, enrollment: string, description: string) {
    return this.requestService
      .putRequest("medico", {
        id: id,
        nombre: name,
        matricula: enrollment,
        descripcion: description
      })
      .map(res => {
        return res;
      });
  }

  getPosition() {
    const medicId = getCurrentUser().asociado.id;
    return this.requestService.getRequest(`medico/${medicId}/ubicacion`, {});
  }

  savePosition(position: string) {
    const medicId = getCurrentUser().asociado.id;
    return this.requestService.postRequest(`medico/${medicId}/ubicacion`, {
      ubicacion: position
    });
  }

  deletePosition() {
    const medicId = getCurrentUser().asociado.id;
    return this.requestService.deleteRequest(`medico/${medicId}/ubicacion`, {});
  }
}
