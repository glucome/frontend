import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class CommonService {
  constructor(private requestService: RequestService) {}

  uploadImage(formData) {
    let user = getCurrentUser();
    return this.requestService.postImage(`imagen/${user.id}`, formData);
  }
}
