import { Injectable } from '@angular/core';

import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class ReportService {
  constructor(private requestService: RequestService) {}

  getReportNumberMeasurements() {
    return this.requestService.getRequest(`reporte/cantidad_mediciones`, {});
  }

  getReportByMoment() {
    return this.requestService.getRequest(`reporte/por_momento`, {});
  }

  getReportByAgeRange() {
    return this.requestService.getRequest(`reporte/por_rango_edad`, {});
  }

  getReportByDiabetesType() {
    return this.requestService.getRequest(`reporte/por_tipo_diabetes`, {});
  }

  getReportByMeasurementType() {
    return this.requestService.getRequest(`reporte/por_tipo_medicion`, {});
  }

  getReportFoodRanking() {
    return this.requestService.getRequest(`reporte/ranking_alimentos`, {});
  }
}
