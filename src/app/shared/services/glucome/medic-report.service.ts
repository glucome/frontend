import { Injectable } from '@angular/core';

import { getCurrentUser } from '../../../helpers/storage';
import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class MedicReportService {
  private medicId: number;

  constructor(private requestService: RequestService) {
    this.medicId = getCurrentUser().asociado.id;
  }

  getReportByMoment() {
    return this.requestService.getRequest(`medico/reporte/${this.medicId}/por_momento`, {});
  }

  getReportByAgeRange() {
    return this.requestService.getRequest(`medico/reporte/${this.medicId}/por_rango_edad`, {});
  }

  getReportByDiabetesType() {
    return this.requestService.getRequest(`medico/reporte/${this.medicId}/por_tipo_diabetes`, {});
  }

  getReportByMeasurementType() {
    return this.requestService.getRequest(`medico/reporte/${this.medicId}/por_tipo_medicion`, {});
  }
}
