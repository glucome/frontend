import { Injectable } from '@angular/core';
import { Message as ChatMessage } from 'ng-chat';
import { Observable } from 'rxjs';

import { RequestService } from '../request.service';

@Injectable({
  providedIn: "root"
})
export class ChatService {
  constructor(private requestService: RequestService) {}

  createChat(patientID: number, medicId: number) {
    return this.requestService.postRequest("chat/crear_chat_medico_paciente", { idUsuarioPaciente: patientID, idUsuarioMedico: medicId });
  }

  changeState(userID: number, online: boolean) {
    return this.requestService.postRequest("chat/cambiar_estado", { idUsuario: userID, online });
  }

  getPatientsChat(medicId: number) {
    return this.requestService.getRequest("chat/pacientes", { idUsuarioMedico: medicId });
  }

  markMessagesAsRead(chatId: number, userId: number, dateSeen: String) {
    return this.requestService.postRequest("chat/marcar_mensajes_como_leidos", {
      idChat: chatId,
      idAutor: userId,
      fechaHoraVisto: dateSeen
    });
  }

  getMessageHistory(chatId: number): Observable<ChatMessage[]> {
    return this.requestService.getRequest("chat/mensajes", { idChat: chatId }).map(res => {
      let messages: ChatMessage[] = [];
      const messagesResponse = res["respuesta"].mensajes;

      for (const messageResponse of messagesResponse) {
        const fromId = messageResponse.autor;
        const toId = messageResponse.destinatario;
        const message = messageResponse.cuerpo;
        const dateSent = messageResponse.fechaHora;
        const dateSeen = messageResponse.fechaHoraVisto;

        messages.push({ fromId, toId, message, dateSent, dateSeen });
      }

      return messages;
    });
  }
}
