import { Injectable } from '@angular/core';

declare var $: any;
@Injectable({ providedIn: "root" })
export class AlertService {
  constructor() {}

  showNotification(type: string, title: string, message: string, url: string) {
    if (!title) title = "";
    if (!message) message = "";

    $.notify(
      {
        title,
        message,
        icon: "fa fa-bell",
        url,
        newest_on_top: true,
        allow_dismiss: true,
        placement: {
          from: "top",
          align: "right"
        }
      },
      {
        type: "minimalist-" + type,
        animate: {
          enter: "animated fadeInDown",
          exit: "animated fadeOutRight"
        },
        template:
          '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-minimalist alert-{0}" role="alert">' +
          '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
          '<i data-notify="icon" class="pull-left" aria-hidden="true"></i>' +
          '<span data-notify="title"> {1}</span>' +
          '<span data-notify="message"> {2}</span>' +
          "</div>"
      }
    );
  }
}
