# Entorno
* Descargar e instalar [NodeJs](https://nodejs.org/es/) (12.0.0).
* Verificar instalación de NodeJS y de npm.
```
node -v
npm -v
```
* Instalar compilador [TypeScript](https://www.typescriptlang.org).
```
npm install -g typescript
```
* Instalar [Angular CLI](https://cli.angular.io/).
```
npm install -g @angular/cli
```
# Instalación
Ejecutar el siguiente comando.
```
npm install
```

# Ejecución
* Ejecutar el siguiente comando.
```
npm start
```

# Spark
* Descargar [Spark](https://spark.apache.org/downloads.html) (Pre-built for Apache Hadoop 2.7 and later).
* Descomprimir Spark en "C:\\" y cambiar el nombre de la carpeta a "spark".
* Descargar de [winutils](https://github.com/steveloughran/winutils) la versión para Hadoop 3.0.0.
* Copiar "bin\winutils.exe" a "C:\spark\bin".
* Añadir a las variables del sistema:
    * "HADOOP_HOME" con el valor "C:\spark"
    * "SPARK_HOME" con el valor "C:\spark\bin"
* Editar la variable PATH y añadir:
    * %HADOOP_HOME%
    * %SPARK_HOME%


# Editor (opcional)
* Descargar e instalar [Visual Studio Code](https://code.visualstudio.com/).
* Instalar las siguientes extensiones:  
[Angular 2 TypeScript Emmet](https://marketplace.visualstudio.com/items?itemName=jakethashi.vscode-angular2-emmet).  
[Angular 8 Snippets - TypeScript, Html, Angular Material, ngRx, RxJS & Flex Layout](https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode).  
[Angular Language Service](https://marketplace.visualstudio.com/items?itemName=Angular.ng-template).  
[angular2-inline](https://marketplace.visualstudio.com/items?itemName=natewallace.angular2-inline).  
[Bootstrap 4, Font awesome 4, Font Awesome 5 Free & Pro snippets](https://marketplace.visualstudio.com/items?itemName=thekalinga.bootstrap4-vscode).
[HTML CSS Support](https://marketplace.visualstudio.com/items?itemName=ecmel.vscode-html-css).  
[JavaScript (ES6) code snippets](https://marketplace.visualstudio.com/items?itemName=xabikos.JavaScriptSnippets).  
[jshint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.jshint).
[Material Icon Theme](https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme).  
[Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode).